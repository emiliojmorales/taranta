Attribute Dial Widget
*********************

A widget that shows the value of an attribute using an animated dial

\ |IMG1|\ 

Widget setting 
==============
When using the widget the user has a couple of options to configure it:

\ |IMG2|\ 

- First, it should select the attribute to use, it accepts all types of scalar attributes.
- It also has a minimum and maximum configuration, that will limit the widget display, for example, if the attribute has a value of 25 and the widget is configured for a maximum of 20, everything above 20 will be displayed as 20.
- The user can also select 3 types of labels to display on the widget, Attribute, Label and Device

\ |IMG3|\ 

- Finally, the user can select either to display the write value or not, if selected a grey knob will appear together with the red knob from the actual value

\ |IMG4|\ 

Widget design
==============

\ |IMG5|\ 

Applying CSS to widget
======================

One can also write css under ``custom css`` section. The css syntax is same as we write for html files

\ |IMG6|\ 

The CSS rules written in ``custom css`` section are applied to the widget as below:

\ |IMG7|\ 


.. bottom of content

.. |IMG1| image:: _static/img/dial.png

.. |IMG2| image:: _static/img/inspect_dial.png

.. |IMG3| image:: _static/img/dial_lable.png

.. |IMG4| image:: _static/img/dial_with_write_value.png

.. |IMG5| image:: _static/img/dial_with_device.png

.. |IMG6| image:: _static/img/dial_custom_css_inspect.png

.. |IMG7| image:: _static/img/dial_custom_css_output.png
