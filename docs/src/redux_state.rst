Redux State structure
=====================

Redux Devices Tab:

\ |IMG4|\

When taranta Devices tab is open a couple of default actions are triggered:

1. ``FETCH_DEVICES_NAME`` action that results in ``FETCH_DEVICE_NAMES_SUCCESS`` or ``FETCH_DEVICE_NAMES_FAILED`` 
in case of success the following structure is added to the redux state:

.. code-block:: javascript

  deviceList: {
    nameList: [
        "dserver/databaseds/2",
        "dserver/starter/17a39f945d53",
        "dserver/tangoaccesscontrol/1",
        "dserver/tangotest/test",
        "dserver/tarantatestdevice/test",
        "sys/access_control/1",
        "sys/database/2",
        "sys/tg_test/1",
        "tango/admin/17a39f945d53",
        "test/tarantatestdevice/1"
    ]
  }

This information is then used to populate DeviceList component:

\ |IMG1|\ 

2. ``FETCH_DATABASE_INFO`` action that results in ``FETCH_DATABASE_INFO_SUCCESS`` or ``FETCH_DATABASE_INFO_FAILED`` 
in case of success the following structure is added to the redux state:

.. code-block:: javascript

  database: {
    info: 'TANGO Database sys/database/2
    Running since 2023-01-16 14:28:23
    Devices defined  = 10
    Devices exported  = 10
    Device servers defined  = 5
    Device servers exported  = 5
    Device properties defined  = 215 [History lgth = 215]
    Class properties defined  = 84 [History lgth = 36]
    Device attribute properties defined  = 0 [History lgth = 0]
    Class attribute properties defined  = 0 [History lgth = 0]
    Object properties defined  = 0 [History lgth = 0]'
  }

This information is then used to populate InfoPage component:

\ |IMG2|\ 

3. When a user clicks in a device, example:

\ |IMG3|\

``FETCH_DEVICE`` action is trigger, that results in ``FETCH_DEVICE_SUCCESS`` or ``FETCH_DEVICE_FAILED`` 
in case of success the following structure is added to the redux state:

.. code-block:: javascript

  devices: {
    "sys/access_control/1": {
        "connected": true,
        "state": "ON",
        "server": {
            "id": "TangoAccessControl/1",
            "host": "17a39f945d53"
        },
        "errors": [],
        "name": "sys/access_control/1"
    }
  },
  attributes: {
  "sys/access_control/1": {
      "state": {
          "name": "state",
          "label": "State",
          "dataformat": "SCALAR",
          "datatype": "DevState",
          "writable": "READ",
          "description": "No description",
          "displevel": "OPERATOR",
          "minvalue": null,
          "maxvalue": null
      },
      "status": {
          "name": "status",
          "label": "Status",
          "dataformat": "SCALAR",
          "datatype": "DevString",
          "writable": "READ",
          "description": "No description",
          "displevel": "OPERATOR",
          "minvalue": null,
          "maxvalue": null
      }
    }
  },
  commands: {
    "sys/access_control/1": {
        "AddAddressForUser": {
            "name": "AddAddressForUser",
            "tag": 0,
            "displevel": "OPERATOR",
            "intype": "DevVarStringArray",
            "intypedesc": "user name, address",
            "outtype": "DevVoid",
            "outtypedesc": "Uninitialised"
        },
        ...
        "State": {
            "name": "State",
            "tag": 0,
            "displevel": "OPERATOR",
            "intype": "DevVoid",
            "intypedesc": "Uninitialised",
            "outtype": "DevState",
            "outtypedesc": "Device state"
        },
        "Status": {
            "name": "Status",
            "tag": 0,
            "displevel": "OPERATOR",
            "intype": "DevVoid",
            "intypedesc": "Uninitialised",
            "outtype": "DevString",
            "outtypedesc": "Device status"
        },
    }
  },
  properties: {
    "sys/access_control/1": {}
  }

This information is then used to populate all the following devices tabs:

\ |IMG5|\

.. |IMG1| image:: _static/img/DeviceList.png
   :width: 330 px
   :height: 225 px

.. |IMG2| image:: _static/img/InfoPage.png
   :width: 447 px
   :height: 344 px

.. |IMG3| image:: _static/img/clickDevice.png
   :width: 314 px
   :height: 313 px

.. |IMG4| image:: _static/img/DevicesTab.png
   :width: 256 px
   :height: 58 px

.. |IMG5| image:: _static/img/DevicesTabs.png
   :width: 516 px
   :height: 133 px