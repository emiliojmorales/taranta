Boolean Display Widget
***********************

The **Boolean Display Widget** enables the user to set boolean values by using a switch button.

Widget setting 
===============

Widget is simple, the user has the ability to select device, attribute and one additional setting:

* Show device name (boolean)

\ |IMG1|\ 

Widget design 
===============

\ |IMG2|\ 

Applying CSS to widget
======================

One can also write css under ``custom css`` section. The css syntax is same as we write for html files

\ |IMG3|\ 

The CSS rules written in ``custom css`` section are applied to the widget as below:

\ |IMG4|\ 


.. bottom of content

.. |IMG1| image:: _static/img/boolean_display_widget_settings.png
   :height: 244 px
   :width: 291 px

.. |IMG2| image:: _static/img/boolean_display_widget.png
   :height: 39 px
   :width: 279 px

.. |IMG3| image:: _static/img/boolean_display_css_setting.png
   :height: 381 px
   :width: 293 px

.. |IMG4| image:: _static/img/boolean_display_css_output.png
   :height: 58 px
   :width: 277 px
