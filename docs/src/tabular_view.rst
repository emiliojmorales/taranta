Tabular View
************

Tabular View allows presentation of information on a bidirectional grid, N rows x M columns. 

Each row presents an instance of devices and each column represents a property of that device, examples of tables are: list of receptors, list of subarrays, list of alarms. Example of columns are names, addresses, LEDs, states, attribute value


Widget setting 
===============

\ |IMG1|\ 

It is possible to customize the widget setting from the inspector panel(in edit mode), as below: 

+-----------------------+-----------------------------------------------------------------------------------------+
|Input value            |Description                                                                              |
+=======================+=========================================================================================+
|Devices                |The list of devices, listed in rows                                                      |
+-----------------------+-----------------------------------------------------------------------------------------+
|Show default attributes|If checked, it show the attributed defined in config.json file in columns                |
+-----------------------+-----------------------------------------------------------------------------------------+
|Show compact tables    |It compacts the table size in order to show more data.                                   |
+-----------------------+-----------------------------------------------------------------------------------------+
|Show bordered tables   |It creates a border for each cell in order to improve the readability                    |
+-----------------------+-----------------------------------------------------------------------------------------+
|Custom attributes      |The list of custom attributes, not defined in config.json file                           |
+-----------------------+-----------------------------------------------------------------------------------------+
|Text color             |Specify the color of text for given widget                                               |
+-----------------------+-----------------------------------------------------------------------------------------+
|Background color       |Specify the Background color for given widget                                            |
+-----------------------+-----------------------------------------------------------------------------------------+
|Text size              |Specify the font size of text for given widget                                           |
+-----------------------+-----------------------------------------------------------------------------------------+

Applying CSS to widget
======================

One can also write css under ``custom css`` section. The css syntax is same as we write for html files


Widget render 
=============

In the image below, it is possible to see an example of the Tabular widget running. It is important to note that: 

- the table header shows the list of attribute names
- list of devices is reported in the first column in bold 
- if the device does not have the attribute listed in the default configuration or custom attributes, it shows the special char '-' instead of the value
- Also table shows special char '-' when a attribute is not available or invalid for a device.
- States are colored using the Tango color convention
- DevEnum attributes display only the value of the label
- it is not possible to change the order of the default or custom attributes

\ |IMG2|\ 

.. bottom of content

.. |IMG1| image:: _static/img/tabular_inspector.png
   :width: 350 px


.. |IMG2| image:: _static/img/tabular_running.png
