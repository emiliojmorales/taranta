Image display widget
********************

A widget that displays tango images

\ |IMG1|\ 

Widget setting 
==============
When using the widget the user has a couple of options to configure it:

\ |IMG2|\ 

- Select the device and attribute to use, it accepts all types of tango images, after it you can click START

\ |IMG3|\ 

- Widget in runMode

.. bottom of content

.. |IMG1| image:: _static/img/image_display_widget.png

.. |IMG2| image:: _static/img/image_display_config.png

.. |IMG3| image:: _static/img/image_widget_use.png
