Image table widget
******************

A widget that displays tango images as a table

\ |IMG1|\ 

Widget setting 
==============
When using the widget the user has a couple of options to configure it:

\ |IMG2|\ 

- Select the device and attribute to use, it accepts all types of tango images
- Can limit the rows and columns to display (pre-defined as 5)
- Can also select either to display some lables or not

\ |IMG3|\ 

- Widget in runMode


.. bottom of content

.. |IMG1| image:: _static/img/image_table_widget.png

.. |IMG2| image:: _static/img/image_table_config.png

.. |IMG3| image:: _static/img/image_table_use.png
