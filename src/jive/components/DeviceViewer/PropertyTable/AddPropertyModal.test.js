import React from "react";
import { render, fireEvent } from "@testing-library/react";
import AddPropertyModal from "./AddPropertyModal";
import '@testing-library/jest-dom'

test("AddPropertyModal component", () => {
  const onClose = jest.fn();
  const onAdd = jest.fn();
  const { getByText, getByPlaceholderText } = render(
    <AddPropertyModal onClose={onClose} onAdd={onAdd} />
  );

  // Check if modal title is correct
  const modalTitle = getByText("Add Property");
  expect(modalTitle).toBeInTheDocument();

  // Check if name input is focused
  const nameInput = getByPlaceholderText("Name");
  expect(nameInput).toBe(document.activeElement);

  // Check if name input value updates correctly
  fireEvent.change(nameInput, { target: { value: "Test Name" } });
  expect(nameInput.value).toBe("Test Name");

  // Check if value input updates correctly
  const valueInput = getByPlaceholderText("Value");
  fireEvent.change(valueInput, { target: { value: "Test Value" } });
  expect(valueInput.value).toBe("Test Value");

  // Check if cancel button closes modal and calls onClose
  const cancelButton = getByText("Cancel");
  fireEvent.click(cancelButton);
  expect(onClose).toHaveBeenCalled();

  // Check if add button calls onAdd with correct parameters
  fireEvent.change(nameInput, { target: { value: "Test Name 2" } });
  fireEvent.change(valueInput, { target: { value: "Test Value 2" } });
  const addButton = getByText("Add");
  fireEvent.click(addButton);
  expect(onAdd).toHaveBeenCalledWith("Test Name 2", "Test Value 2");
});
