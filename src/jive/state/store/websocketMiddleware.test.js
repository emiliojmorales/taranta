import { websocketMiddleware } from './websocketMiddleware';
import { WEBSOCKET } from '../actions/actionTypes';

const mockStore = {
  dispatch: jest.fn(),
};

describe('websocketMiddleware', () => {
  let middleware;
  let next;

  beforeEach(() => {
    middleware = websocketMiddleware(mockStore);
    next = jest.fn();
  });

  it('subscribes to the websocket on WS_SUBSCRIBE', async () => {
    const action = { type: WEBSOCKET.WS_SUBSCRIBE, payload: { devices: ['device1', 'device2'] } };
    await middleware(next)(action);
    expect(next).toHaveBeenCalledWith(action);
  });

  it('executes a command on WS_EXECUTE_COMMAND', async () => {
    const action = {
      type: WEBSOCKET.WS_EXECUTE_COMMAND,
      payload: {
        device: 'device',
        command: 'command',
        argin: 'argin',
        id: 'id'
      },
    };
    await middleware(next)(action);
    expect(mockStore.dispatch).toHaveBeenCalledWith({
      type: WEBSOCKET.WS_EXECUTE_COMMAND_SUCCESS,
      id: 'id',
      device: 'device',
      command: 'command',
      output: 'Error',
    });
  });

  it('passes the action to the next middleware if not WS_SUBSCRIBE or WS_EXECUTE_COMMAND', async () => {
    const action = { type: 'OTHER_ACTION' };
    await middleware(next)(action);
    expect(next).toHaveBeenCalledWith(action);
  });

  describe('websocketMiddleware', () => {
    it('unsubscribes from the subscription on window beforeunload', () => {
      let subscription = {
        unsubscribe: jest.fn()
      };
      
      window.addEventListener = jest.fn((event, cb) => {
        if (event === 'beforeunload') {
          cb();
        }
      });
      
      websocketMiddleware({ dispatch: jest.fn() })(jest.fn())({ type: WEBSOCKET.WS_SUBSCRIBE, payload: {devices: []} });
      
      expect(window.addEventListener).toHaveBeenCalledWith('beforeunload', expect.any(Function));
    });
  });

});