import config from "../../../config.json";
import { WEBSOCKET } from "../actions/actionTypes";
import { ATTRIBUTES_SUB_WITH_VALUES_AND_TIMESTAMP } from '../../../shared/api/graphqlQuery'
import UserActions from "../../../shared/api/UserActions";
import { fetchInitialValues } from "../../../shared/utils/fetchInitialValues";
import { WebSocketLink } from 'apollo-link-ws';
import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { onError } from 'apollo-link-error';
import { ApolloLink } from 'apollo-link';
import gql from 'graphql-tag'

function socketUrl(): string {
  const loc = window.location;
  const protocol = loc.protocol.replace("http", "ws");
  const url = loc.href.replace(config.basename,"").split('/');
  return (
    protocol +
    "//" +
    loc.host +
    config.basename +
    "/" +
    url[3] +
    "/socket"
  );
}

const wsLink = new WebSocketLink(
  {
    uri: socketUrl(),
    options: {
    reconnect: true
    }
  }
);

// error handling link
const errorLink = onError(({ graphQLErrors, networkError }) => {
  if (graphQLErrors)
    graphQLErrors.map(({ message, locations, path }) =>
      console.log(
        `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`
      )
    );
  if (networkError) console.log(`[Network error]: ${networkError}`);
});

// create the apollo client
const client = new ApolloClient({
  link: ApolloLink.from([errorLink, wsLink]),
  cache: new InMemoryCache()
});

let subscription;

export const websocketMiddleware = (store) => (next) => async (action) => {

  const userAction = new UserActions(socketUrl());

  window.addEventListener("beforeunload", async () => {
    if (subscription) subscription.unsubscribe();
    if(client) client.stop();
  });

  if (action.type === WEBSOCKET.WS_SUBSCRIBE) {

    if (subscription) subscription.unsubscribe();

    const fullNames = action.payload["devices"];

    //fetch initial attributes state and sub only to state attribute
    fullNames.forEach(async element => {
      fetchInitialValues(store.dispatch, element);
    });

    const subscriptionObservable = client.subscribe({
      query: gql(ATTRIBUTES_SUB_WITH_VALUES_AND_TIMESTAMP),
      variables: {
        fullNames
      }
    });

    // subscribe to the updates
    subscription = subscriptionObservable.subscribe({
      next: ({ data }) => {
        store.dispatch({
          type: WEBSOCKET.WS_MESSAGE,
          value: [{ data: JSON.stringify({ type: "data", payload: { data } }) }]
        });
      }
    });
  }
  else if (
    action.type &&
    action.type === WEBSOCKET.WS_EXECUTE_COMMAND
  ) {
    const { device, command, argin, id } = action?.payload;
    const variables = {
      device,
      command,
      argin,
    };
    //Execute command on the device
    const res = await userAction.executeCommand(variables);
    let output = res && res?.ok ? res?.output : "Error";

    //Dispatch data to the store
    store.dispatch({
      type: WEBSOCKET.WS_EXECUTE_COMMAND_SUCCESS,
      id,
      device,
      command,
      output,
    });
  }

  return next(action);
}