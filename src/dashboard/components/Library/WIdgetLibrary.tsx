import React, { Component } from "react";

import LibraryWidget from "./LibraryWidget";
import { bundles } from "../../widgets";
import config from '../../../config.json';

interface Props {
  render: boolean;
}
export default class WidgetLibrary extends Component<Props> {
  render() {
    if (!this.props.render) {
      return null;
    }
    return (
      <div className="Library">
        {bundles.map((bundle, i) => {
          // Check if the current URL is accepted to include ELASTICSEARCH_LOG_VIEWER
          if (!config?.ELASTIC_ACCEPTED_URLS.includes(window.location.origin) &&
            bundle.definition.type === 'ELASTICSEARCH_LOG_VIEWER')
            return null;
          else return <LibraryWidget key={i} bundle={bundle} />;
        })}
      </div>
    );
  }
}
