import React from "react";

import { configure, shallow, mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import AttributeSelect from "./AttributeSelect";
import TangoAPI from "../../../shared/api/tangoAPI";
import AttributeSuggester from "./AttributeSuggester";
import DeviceSuggester from "./DeviceSuggester";

configure({ adapter: new Adapter() });
jest.mock("./AttributeSuggester");
jest.mock("./DeviceSuggester");

describe("AttributeSelectTests", () => {
    beforeEach(() => {
      jest.resetAllMocks();
    })

  it("renders device and attribute suggestion boxes on initial render", async () => {
      let tangoDB = "MockDB";
      TangoAPI.fetchDeviceAttributes = jest.fn().mockReturnValue([])
      const element = await shallow(<AttributeSelect tangoDB={tangoDB} />);
      element.html();
      expect(DeviceSuggester).toHaveBeenCalledTimes(1);
      expect(AttributeSuggester).toHaveBeenCalledTimes(1);
  })

  it("handles device selection", async () => {
      jest.resetAllMocks();
      TangoAPI.fetchDeviceAttributes = jest.fn().mockReturnValue([])
      let tangoDB = "MockDB";
      let device = "sys/tg_test/1";
      let onSelect = jest.fn();

    const element = await shallow(<AttributeSelect tangoDB={tangoDB} onSelect={onSelect} />);
    const instance = element.instance();
    // @ts-ignore
    await instance.handleSelectDevice(device);
    expect(onSelect).toHaveBeenCalledWith(device, null, null);
  });

  it("handles attribute selection", async () => {
      jest.resetAllMocks();
      let attr1 = {
        name: "short_scalar",
        label: "ShortScalar",
        dataformat: "SCALAR"
      }
      let attr2 = {
        name: "short_spectrum",
        label: "ShortSpectrum",
        datatype: "numeric",
        dataformat: "SPECTRUM"
      }
      TangoAPI.fetchDeviceAttributes = jest.fn().mockReturnValue([attr1, attr2])
      let tangoDB = "MockDB";
      let device = "sys/tg_test/1";
      let onSelect = jest.fn();

    const element = await shallow(<AttributeSelect tangoDB={tangoDB} device={device} onSelect={onSelect} />);
    const instance = element.instance();
    // @ts-ignore
    await instance.handleSelectAttribute(attr1["label"]);
    expect(onSelect).toHaveBeenCalledWith(device, attr1["name"], attr1["label"]);
  });

  it("fetches attributes from TangoAPI", async () => {
      jest.resetAllMocks();
      let attr1 = {
        name: "short_scalar",
        label: "ShortScalar",
        datatype: "DevUShort",
        dataformat: "SCALAR"
      }
      let attr2 = {
        name: "short_spectrum",
        label: "ShortSpectrum",
        datatype: "DevUShort",
        dataformat: "SPECTRUM"
      }
      TangoAPI.fetchDeviceAttributes = jest.fn().mockReturnValue([attr1, attr2]);
      let tangoDB = "MockDB";
      let device = "sys/tg_test/1";
      let onSelect = jest.fn();
    const element = await shallow(<AttributeSelect tangoDB={tangoDB} device={device} onSelect={onSelect} />);
    element.html();
    expect(TangoAPI.fetchDeviceAttributes).toHaveBeenCalledWith(tangoDB, device);
    let filtered_attributes = element.state()["attributes"];
    expect(filtered_attributes.length).toBe(2);
  });

  it("filters attributes by data format", async () => {
      jest.resetAllMocks();
      let attr1 = {
        name: "short_scalar",
        label: "ShortScalar",
        datatype: "DevUShort",
        dataformat: "SCALAR"
      }
      let attr2 = {
        name: "short_spectrum",
        label: "ShortSpectrum",
        datatype: "DevUShort",
        dataformat: "SPECTRUM"
      }
      TangoAPI.fetchDeviceAttributes = jest.fn().mockReturnValue([attr1, attr2]);
      let tangoDB = "MockDB";
      let device = "sys/tg_test/1";
      let dataFormat: "scalar" | "spectrum" | "image" = "spectrum";
      let onSelect = jest.fn();
    const element = await shallow(<AttributeSelect tangoDB={tangoDB} device={device} onSelect={onSelect} dataFormat={dataFormat}/>);
    element.html();
    const expectedCall = {"labels":  [attr2["label"]], "attributes": [attr2["name"]]};
    expect(AttributeSuggester).toHaveBeenCalledWith(expect.objectContaining(expectedCall), expect.anything(), expect.anything());
  });

  it("filters attributes by data type", async () => {
      jest.resetAllMocks();
      let attr1 = {
        name: "state",
        label: "State",
        datatype: "DevState",
        dataformat: "SCALAR"
      }
      let attr2 = {
        name: "short_scalar",
        label: "ShortScalar",
        datatype: "DevUShort",
        dataformat: "SCALAR"
      }
      TangoAPI.fetchDeviceAttributes = jest.fn().mockReturnValue([attr1, attr2]);
      let tangoDB = "MockDB";
      let device = "sys/tg_test/1";
      let dataType: "numeric" = "numeric";
      let dataFormat: "scalar" | "spectrum" | "image" = "scalar";
      let onSelect = jest.fn();
    const element = await shallow(<AttributeSelect tangoDB={tangoDB} device={device} onSelect={onSelect} dataType={dataType} dataFormat={dataFormat}/>);
    element.html();
    const expectedCall = {"labels":  [attr2["label"]], "attributes": [attr2["name"]]};
    expect(AttributeSuggester).toHaveBeenCalledWith(expect.objectContaining(expectedCall), expect.anything(), expect.anything());
  });

  it("Tabular widget: Filter duplicate attributes for multi device", async () => {
    jest.resetAllMocks();
    let attr1 = {
      name: "state",
      label: "State",
      datatype: "DevState",
      dataformat: "SCALAR"
    }
    let attr2 = {
      name: "short_scalar",
      label: "ShortScalar",
      datatype: "DevUShort",
      dataformat: "SCALAR"
    }
    let attr3 = {
      name: "short_scalar",
      label: "ShortScalar",
      datatype: "DevUShort",
      dataformat: "SCALAR"
    }
    TangoAPI.fetchMultipleDevicesAttributes = jest.fn().mockReturnValue([attr1, attr2, attr3]);
    let tangoDB = "MockDB";
    let device = "sys/tg_test/1,test/tarantatestdevice/1";
    let dataType: "numeric" = "numeric";
    let dataFormat: "scalar" | "spectrum" | "image" = "scalar";
    let onSelect = jest.fn();

    const element = await shallow(<AttributeSelect tangoDB={tangoDB} device={device} onSelect={onSelect} dataType={dataType} dataFormat={dataFormat}/>);
    //Duplicate attribute will be filtered
    expect(element.state()['attributes']).toEqual([attr1, attr2]);
  });

});
