import React from "react";
import { AttributeInput, ComplexInputDefinition } from "../types";

import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import Spectrum2D from "./Spectrum2D";

interface AttributeComplexInput {
    attribute: AttributeInput;
  }
interface Input {
    attributeX: AttributeInput;
    attributes: ComplexInputDefinition;
    showTitle: boolean;
    showAttribute: string;
    xScientificNotation: boolean;
    yScientificNotation: boolean;
    xLogarithmicScale: boolean;
    yLogarithmicScale: boolean;
    plotStyling: string;
  }

configure({ adapter: new Adapter() });

describe("Spectrum2D Tests", () => {
    let myAttributeInputX: AttributeInput;
    let myAttributeInputY: ComplexInputDefinition;
    let myInput: Input;
    var writeArray: any = [];
    var date = new Date();
    var timestamp = date.getTime();
  
    it("renders library mode true without crashing with logarithmic", () => {
        myAttributeInputX = {
            device: "sys/tg_test/1",
            attribute: "time_range",
            label: "TimeRange",
            history: [],
            dataType: "",
            dataFormat: "",
            isNumeric: true,
            unit: "",
            enumlabels: [],
            write: writeArray,
            value: [1, 2, 3, 4, 5],
            writeValue: "",
            timestamp: timestamp
          };
          myAttributeInputY = {
            type: "complex",
            inputs: {
                device: "sys/tg_test/1",
                attribute: "voltage",
                label: "Voltage",
                history: [],
                dataType: "",
                dataFormat: "",
                isNumeric: true,
                unit: "",
                enumlabels: [],
                write: writeArray,
                value: [0, 1, 2, 3, 4],
                writeValue: "",
                timestamp: timestamp
              }
          };
          myInput = {
            attributeX: myAttributeInputX,
            attributes: myAttributeInputY,
            showTitle: true,
            showAttribute: "Name",
            xScientificNotation: false,
            yScientificNotation: false,
            xLogarithmicScale: true,
            yLogarithmicScale: true,
            plotStyling: "bar",
            };
  
      const element = React.createElement(Spectrum2D.component, {
        mode: "library",
        t0: 1,
        actualWidth: 100,
        actualHeight: 100,
        inputs: myInput
      });
      expect(shallow(element).html()).toContain("height:150px")
    });

    it("renders library mode true without crashing with exponent", () => {
        myAttributeInputX = {
            device: "sys/tg_test/1",
            attribute: "time_range",
            label: "TimeRange",
            history: [],
            dataType: "",
            dataFormat: "",
            isNumeric: true,
            unit: "",
            enumlabels: [],
            write: writeArray,
            value: [1, 2, 3, 4, 5],
            writeValue: "",
            timestamp: timestamp
          };
          myAttributeInputY = {
            type: "complex",
            inputs: {
                device: "sys/tg_test/1",
                attribute: "voltage",
                label: "Voltage",
                history: [],
                dataType: "",
                dataFormat: "",
                isNumeric: true,
                unit: "",
                enumlabels: [],
                write: writeArray,
                value: [0, 1, 2, 3, 4],
                writeValue: "",
                timestamp: timestamp
              }
          };
          myInput = {
            attributeX: myAttributeInputX,
            attributes: myAttributeInputY,
            showTitle: true,
            showAttribute: "Name",
            xScientificNotation: true,
            yScientificNotation: true,
            xLogarithmicScale: false,
            yLogarithmicScale: false,
            plotStyling: "markers",
            };
  
      const element = React.createElement(Spectrum2D.component, {
        mode: "run",
        t0: 1,
        actualWidth: 100,
        actualHeight: 100,
        inputs: myInput
      });
      expect(shallow(element).html()).not.toContain("height:150px")
    });

  });