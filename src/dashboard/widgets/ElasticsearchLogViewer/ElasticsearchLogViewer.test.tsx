import React from "react";
import { configure, mount, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { Provider } from "react-redux";
import store from "../../state/store";
import WrapperElasticsearchLogViewer, { ConnectedElasticsearchLogViewer } from "./ElasticsearchLogViewer"

configure({ adapter: new Adapter() });

interface Input {
    elasticsearchUrl: string;
    selectedRefresh?: number;
}

describe("Test Elasticsearch log viewer", () => {
    let myInput: Input;
    const selectedDashboard = { "selectedId": null, "selectedIds": ["1"], "widgets": { "1": { "_id": "60910a16464196001183c1c4", "id": "1", "x": 16, "y": 3, "canvas": "0", "width": 13, "height": 4, "type": "PARAMETRIC_WIDGET", "inputs": { "name": "MName", "variable": "myvar" }, "order": 0, "valid": true } }, "id": "604b5fac8755940011efeea1", "name": "Untitled dashboard", "user": "user1", "group": null, "groupWriteAccess": false, "lastUpdatedBy": "user1", "insertTime": "2021-03-12T12:33:48.981Z", "updateTime": "2021-05-05T15:07:22.173Z", "history": { "undoActions": [], "redoActions": [], "undoIndex": 0, "redoIndex": 0, "undoLength": 0, "redoLength": 0 }, "variables": [] }
    const dashboards = [{ "id": "604b5fac8755940011efeea1", "name": "Untitled dashboard", "user": "user1", "insertTime": "2021-03-12T12:33:48.981Z", "updateTime": "2021-05-05T15:07:22.173Z", "group": null, "lastUpdatedBy": "user1", "tangoDB": "testdb", "variables": [{ "id": "0cef8i6340ij7", "name": "myvar", "class": "DServer", "device": "dserver/DataBaseds/2" }] }, { "id": "6073154703c08b0018111066", "name": "ajay", "user": "user1", "insertTime": "2021-04-11T15:27:03.803Z", "updateTime": "2021-05-04T13:01:40.904Z", "group": null, "lastUpdatedBy": "user1", "tangoDB": "testdb" }];

    beforeEach(() => {
        // Mock the window.location object
        delete window.location;
        window.location = new URL('https://k8s.stfc.skao.int/testdb/dashboard?id=64255c0fdd11d00018977284');
      });

    it("it renders without crash in edit mode", () => {
        myInput = {
            elasticsearchUrl: ""
        }

        let element = React.createElement(WrapperElasticsearchLogViewer.component, {
            mode: "edit",
            t0: 1,
            actualWidth: 100,
            actualHeight: 100,
            inputs: myInput
        })

        const content = mount(<Provider store={store}>{element}</Provider>);
        content.setProps({
            username: "CREAM",
            selectedDashboard: selectedDashboard,
            dashboards: dashboards,
        });

        expect(content.html()).toContain("log-container");
    });

    it("it renders without crash in run mode", () => {
        myInput = {
            elasticsearchUrl: ""
        }

        let element = React.createElement(WrapperElasticsearchLogViewer.component, {
            mode: "run",
            t0: 1,
            actualWidth: 100,
            actualHeight: 100,
            inputs: myInput
        })

        const content = mount(<Provider store={store}>{element}</Provider>);
        content.setProps({
            username: "CREAM",
            selectedDashboard: selectedDashboard,
            dashboards: dashboards,
        });

        expect(content.html()).toContain("fa-pause-circle");;
    });

    it("it renders without crash in run mode with custom elasticsearchUrl", () => {
        myInput = {
            elasticsearchUrl: "http://elastic:CREAM_SKA@192.168.1.1:9200"
        }

        let element = React.createElement(WrapperElasticsearchLogViewer.component, {
            mode: "run",
            t0: 1,
            actualWidth: 100,
            actualHeight: 100,
            inputs: myInput
        })

        const content = mount(<Provider store={store}>{element}</Provider>);
        content.setProps({
            username: "CREAM",
            selectedDashboard: selectedDashboard,
            dashboards: dashboards,
        });

        expect(content.html()).toContain("fa-pause-circle");
        // expect(content.html()).toContain("Select log level");
    })

    it("check refresh rate", async () => {
        myInput = {
            elasticsearchUrl: "http://elastic:CREAM_SKA@192.168.1.1:9200",
            selectedRefresh: 1000,
        }
        const launchElasticQuery = jest.fn();

        let element = React.createElement(ConnectedElasticsearchLogViewer.WrappedComponent, {
            mode: "run",
            t0: 1,
            actualWidth: 100,
            actualHeight: 100,
            inputs: myInput,
            messages: {},
            fetchElasticLogs: jest.fn()
        })

        const shallowElement = shallow(element);
        shallowElement.setProps({
            username: "CREAM",
            selectedDashboard: selectedDashboard,
            dashboards: dashboards,
        });

        shallowElement.instance().launchElasticQuery = launchElasticQuery;
        //change the refresh rate from 1 sec to 5 sec
        // shallowElement.find('.fa-angle-down').simulate('click');
        shallowElement.find('#refresh-rate').simulate('change', {
            currentTarget: { "label": "5sec", "name": "5 seconds", "value": "5000" }
        });
        //check the changes
        expect(shallowElement.state('selectedRefresh')).toBe(5000);
        expect(launchElasticQuery).toHaveBeenCalledTimes(1);
        // expect(content.find('#refresh-rate').find('option').at(1).html()).toContain('3sec');
        // content.find('ElasticsearchLogViewer').update();
    })
})