import React from "react";
import { AttributeInput } from "../types";

import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import Spectrum from "./Spectrum";

interface Input {
  attribute: AttributeInput;
  showTitle: boolean;
  inelastic: boolean;
  showAttribute: string;
}

configure({ adapter: new Adapter() });

describe("Spectrum Tests", () => {
  let myAttributeInput: AttributeInput;
  let myInput: Input;
  var writeArray: any = [];
  var date = new Date();
  var timestamp = date.getTime();

  beforeEach(() => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "time_range",
      label: "TimeRange",
      isNumeric: true,
      write: writeArray,
      value: [1, 2, 3, 4, 5],
      timestamp: timestamp,
    };
    myInput = {
      attribute: myAttributeInput,
      showTitle: true,
      inelastic: true,
      showAttribute: "Name",
    };
  })

  it("renders library mode true without crashing with inelastic axis", () => {
    const element = React.createElement(Spectrum.component, {
      mode: "library",
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });
    expect(shallow(element).html()).toContain("height:150px");
  });

  it("renders run mode true without crashing with elastic axis", () => {
    myInput = {
      attribute: myAttributeInput,
      showTitle: true,
      inelastic: false,
      showAttribute: "Label",
    };

    const element = React.createElement(Spectrum.component, {
      mode: "run",
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });
    expect(shallow(element).html()).toContain("height:100px");
  });

  it("does not update state when mode is not 'run'", () => {
    const element = React.createElement(Spectrum.component, {
      mode: "library",
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });

    const shallowElement = shallow(element);
    shallowElement.setState({ min: 1, max: 5 });
    shallowElement.instance().componentDidUpdate();
    expect(shallowElement.state("min")).toEqual(1);
    expect(shallowElement.state("max")).toEqual(5);
  });

  it("updates state when mode is 'run' with correct min and max values", () => {
    const element = React.createElement(Spectrum.component, {
      mode: "run",
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });

    const propsInput = {
      myInput,
      attribute: {
        myAttributeInput,
        value: [-1, 0, 6, 7, 8]
      }
    };

    const shallowElement = shallow(element);
    shallowElement.setState({ min: 1, max: 5 });
    shallowElement.setProps({ inputs: propsInput });
    shallowElement.instance().componentDidUpdate();
    expect(shallowElement.state("min")).toEqual(-1);
    expect(shallowElement.state("max")).toEqual(8);
  });
});

