import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import Label from "./Label";

configure({ adapter: new Adapter() });

function input(text = "My Link",
    linkTo = "http://myLink.com",
    backgroundColor = "#ffffff",
    textColor = "#000000",
    size = 1,
    font = "Helvetica",
    borderWidth = 0,
    borderColor = "#000000",
    customCss = "") {
    return {
        "text": text,
        "linkTo": linkTo,
        "backgroundColor": backgroundColor,
        "textColor": textColor,
        "size": size,
        "font": font,
        "borderWidth": borderWidth,
        "borderColor": borderColor,
        "customCss": customCss
    }
}

describe("Label", () => {
    it("renders attributes correctly", () => {
        let element = React.createElement(Label.component, {
            mode: "run", t0: 1, actualWidth: 25,
            actualHeight: 25,
            inputs: input()
        });
        expect(shallow(element).html()).toContain("My Link");
        expect(shallow(element).html()).toContain("http://myLink.com");
    });

    it("renders label with italic style when text is 'empty' and mode is 'edit'", () => {
        let element = React.createElement(Label.component, {
            mode: "edit",
            t0: 1,
            actualWidth: 25,
            actualHeight: 25,
            inputs: input("")
        });
        expect(shallow(element).html()).toContain('<span style=\"font-style:italic\">Empty</span>');
    });

    it("renders label when mode is 'library'", () => {
        let element = React.createElement(Label.component, {
            mode: "library",
            t0: 1,
            actualWidth: 25,
            actualHeight: 25,
            inputs: input()
        });
        expect(shallow(element).html()).toContain('<span>Label</span>');
    });
});
