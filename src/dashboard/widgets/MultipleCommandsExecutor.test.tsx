import React, { ReactElement } from "react";
import { configure, shallow, mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import MultipleCommandsExecutor from "./MultipleCommandsExecutor";
import { CommandInputMultiple } from "../types";

interface Input {
  title: string;
  requireConfirmation: boolean;
  command: CommandInputMultiple;
  displayOutput: boolean;
  cooldown: number;
  textColor: string;
  backgroundColor: string;
  size: number;
  font: string;
};

configure({ adapter: new Adapter() });

function timeout(timeout: number) {
  return new Promise(resolve => setTimeout(resolve, timeout));
}

describe("MultipleCommandsExecutor Widget Tests", () => {
  let myCommandInput: CommandInputMultiple;
  let myInput: Input
  let element: ReactElement;

  function getReactElement(mode: string, myInput: Input): ReactElement {
    return React.createElement(MultipleCommandsExecutor.component, {
      mode: mode,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });
  }

  beforeEach(() => {
    myCommandInput = {
      device: "sys/tg_test/1",
      command: ["DevString1", "DevString2"],
      output: ["Hello1", "Hello2"],
      execute: jest.fn()
    };

    myInput = {
      title: "MultipleCommandsExecutor",
      requireConfirmation: true,
      command: myCommandInput,
      displayOutput: true,
      cooldown: 0,
      textColor: "#000",
      backgroundColor: "#ffffff",
      size: 1,
      font: "Helvetica",
    };
  });

  afterEach(() => {
    jest.restoreAllMocks();
  });

  it("renders title with run and library modes", () => {
    element = getReactElement("run", myInput);
    expect(shallow(element).html()).toContain("MultipleCommandsExecutor");

    element = getReactElement("library", myInput);
    expect(shallow(element).html()).toContain("MultipleCommandsExecutor");
  });

  it('renders buttons for multiple commands', () => {
    element = getReactElement("run", myInput);
    const mountedElement = mount(element);
    const buttons = mountedElement.find('button');
    expect(buttons.length).toBe(2);
    expect(buttons.at(0).text()).toBe('DevString1');
    expect(buttons.at(1).text()).toBe('DevString2');
  });

  it('handles command execution when confirmation is not required', async () => {
    myInput.command.output = myInput.command.command;
    element = getReactElement("run", myInput);
    const mountedElement = mount(element);
    const button = mountedElement.find('button').at(0);
    window.confirm = jest.fn(() => true);
    button.simulate('click');
    await timeout(0);
    expect(myInput.command.execute).toHaveBeenCalledTimes(1);
    expect(myInput.command.execute).toHaveBeenCalledWith(null, 'DevString1');
  });

  it('handles cooldown correctly', async () => {
    myInput.command.output = myInput.command.command;
    myInput.cooldown = 1;
    element = getReactElement("run", myInput);
    const mountedElement = mount(element);
    const button = mountedElement.find('button').at(0);
    window.confirm = jest.fn(() => true);
    button.simulate('click');
    expect(mountedElement.state('cooldown')).toBe(true);

    await timeout(1000 * myInput.cooldown);
    expect(mountedElement.state('cooldown')).toBe(false);
    expect(myInput.command.execute).toHaveBeenCalledTimes(1);
    expect(myInput.command.execute).toHaveBeenCalledWith(null, 'DevString1');
  });
});