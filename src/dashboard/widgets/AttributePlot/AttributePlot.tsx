import React from "react";

import Plot, { Trace } from "./Plot";
import { WidgetProps } from "../types";
import { Inputs } from ".";

type Props = WidgetProps<Inputs>;
type AttributeEntries = Props["inputs"]["attributes"];

function AttributePlot(props: Props) {
  const { mode, inputs, actualWidth, actualHeight } = props;
  const { attributes, timeWindow, showZeroLine, logarithmic, textColor, backgroundColor } = inputs;

  const runParams = {
    width: actualWidth,
    height: actualHeight,
    timeWindow,
    showZeroLine,
    logarithmic,
    textColor,
    backgroundColor
  };

  const staticParams = { ...runParams, staticMode: true };

  if (mode === "run") {
    const traces = tracesFromAttributeInputs(attributes, props.t0);
    return <Plot traces={traces} params={runParams} mode = {mode} />;
  }

  if (mode === "library") {
    const xValues = Array(timeWindow)
      .fill(0)
      .map((_, i) => i);
    const sample1 = xValues.map(x => 8 * Math.sin(x / 6) * Math.sin(x / 20));
    const sample2 = xValues.map(x => 5 * Math.cos(x / 20) * Math.cos(x / 3));
    const traces: Trace[] = [
      {
        fullName: "attribute 1",
        x: xValues,
        y: sample1,
        axisLocation: "left",
        lineColor: "#000000"
      },
      {
        fullName: "attribute 2",
        x: xValues,
        y: sample2,
        axisLocation: "left",
        lineColor: "#000000"
      }
    ];

    return <Plot traces={traces} params={{ ...staticParams, height: 150 }} mode = {mode} />;
  } else {
    const traces = attributes.map(attributeInput => {
      const { device, attribute, label } = attributeInput.attribute;
      const { showAttribute } = attributeInput;
      
      let display = "";
      if(showAttribute === "Label") {
        if(label!=="") display = label;
        else display = "attributeLabel";
      } 
      else if(showAttribute === "Name"){
        if(attribute!==null) display = attribute;
        else display = "attributeName";
      } 
      
      const fullName = `${device || "?"}/${display || "?"}`;
      const trace: Trace = { fullName, axisLocation: attributeInput.yAxis, lineColor: attributeInput.lineColor };
      return trace;
    });

    return <Plot traces={traces} params={staticParams} mode = {props.mode} />;
  }
}

function tracesFromAttributeInputs(
  complexInputs: AttributeEntries,
  t0: number
): Trace[] {
  return complexInputs.map(complexInput => {
    const { attribute: attributeInput, yAxis, lineColor } = complexInput;
    const { history, device, attribute } = attributeInput;
    const fullName = `${device}/${attribute}`;

    let x: number[] = [];
    let y: number[] = [];

    if (history.length > 0) {
      x = history.map(({ timestamp }) => timestamp - t0);
      y = history.map(({ value }) => value);
    }

    return { fullName, x, y, axisLocation: yAxis, lineColor };
  });
}


export default AttributePlot;
