import React from "react";
import { DeviceInput, AttributeInput } from "../types";

import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import AttributeDial from "./AttributeDial";

configure({ adapter: new Adapter() });

describe("DeviceStatus", () => {
  let myDeviceInput:DeviceInput;
  myDeviceInput = {
    alias: "",
    name: "sys/tg_test/1"
  }
  let myAttributeInput: AttributeInput;
  myAttributeInput = {
    device: "sys/tg_test/1",
    attribute: "double_scalar",
    label: "double_scalar",
    value: 8.5,
    writeValue: 0,
    timestamp: 1668079933.435179,
    dataFormat: "scalar",
    dataType: "DevDouble",
    enumlabels: [],
    history: [],
    isNumeric: true,
    unit: "",
    write: () => {},
  }
  it("AttributeDial: Check for custom CSS ", () => {

    const element = React.createElement(AttributeDial.component, {
      mode: "run",
      t0: 1,
      actualWidth: 500,
      actualHeight: 140,
      inputs: {
        widgetCss: "background-color: cyan\nborder: 2px dashed red;\nfont-size: 22px;",
        device: myDeviceInput,
        attribute: myAttributeInput,
        label: "attribute",
        min: -250,
        max: 250,
      }
    });
    const shallowElement = shallow(element);
    const elemNoWhiteSpace = shallowElement.html().replace(/\s/g, '');
    expect(elemNoWhiteSpace).toContain("background-color:cyan");
    expect(elemNoWhiteSpace).toContain("font-size:22px");
    expect(elemNoWhiteSpace).toContain("double_scalar");

  });

  it("renders with parameters correctly read", () => {
    myAttributeInput.value = undefined;

    const element = React.createElement(AttributeDial.component, {
      mode: "library",
      t0: 1,
      actualWidth: 500,
      actualHeight: 140,
      inputs: {
        widgetCss: "background-color: cyan\nborder: 2px dashed red;\nfont-size: 22px;",
        device: myDeviceInput,
        attribute: myAttributeInput,
        label: "aattribute",
        min: -250,
        max: 250,
      }
    });

    const shallowElement = shallow(element);
    const elemNoWhiteSpace = shallowElement.html().replace(/\s/g, '');
    expect(elemNoWhiteSpace).toContain("width:60px");
    expect(elemNoWhiteSpace).toContain("aattribute");
  });

})