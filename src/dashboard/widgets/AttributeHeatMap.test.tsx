import React, { ReactElement } from "react";
import { configure, mount, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import AttributeHeatMap from "./AttributeHeatMap";
import { AttributeInput } from "../types";

interface Input {
  attribute: AttributeInput,
  xAxis: AttributeInput,
  yAxis: AttributeInput,
  showTitle: boolean,
  selectAxisAttribute: boolean,
  showAttribute: string,
  fixedScale: boolean,
  maxValue: number,
  minValue: number
};

configure({ adapter: new Adapter() });

describe("AttributeHeatMap Widget Tests", () => {
  let myAttributeInput: AttributeInput;
  let myInput: Input;
  let xAxisInput: AttributeInput, yAxisInput: AttributeInput;
  var writeArray: any = [];
  var date = new Date();
  let element: ReactElement;

  function getReactElement(mode: string, myInput: Input): ReactElement {
    return React.createElement(AttributeHeatMap.component, {
      mode: mode,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });
  }

  beforeEach(() => {
    xAxisInput = {
      device: "sys/tg_test/x",
      attribute: "x-axis",
      label: "x-axis",
      isNumeric: true,
      write: writeArray,
      value: [1, 2, 3],
      timestamp: date.getTime(),
    };

    yAxisInput = {
      device: "sys/tg_test/y",
      attribute: "y-axis",
      label: "y-axis",
      isNumeric: true,
      write: writeArray,
      value: [1, 2, 3],
      timestamp: date.getTime(),
    };

    myAttributeInput = {
      device: "sys/tg_test/Input",
      attribute: "Image",
      label: "Image",
      isNumeric: true,
      write: writeArray,
      value: [
        [30, 60, 1],
        [20, 1, 60],
        [1, 20, 30],
      ],
      timestamp: date.getTime(),
    }

    myInput = {
      attribute: myAttributeInput,
      xAxis: xAxisInput,
      yAxis: yAxisInput,
      showTitle: true,
      selectAxisAttribute: false,
      showAttribute: "Label",
      fixedScale: true,
      maxValue: 30,
      minValue: 1,
    }
  });

  afterEach(() => {
    jest.restoreAllMocks();
  });

  it("renders with run and library modes", () => {
    element = getReactElement("run", myInput);
    expect(shallow(element).html()).toContain("height:100px");

    element = getReactElement("library", myInput);
    expect(shallow(element).html()).toContain("height:150px");
  });

  it("renders with correct title", () => {
    element = getReactElement("run", myInput);
    expect(shallow(element).instance().getTitle()).toEqual("sys/tg_test/Input/Image");
    myInput.showTitle = false;

    element = getReactElement("run", myInput);
    expect(shallow(element).instance().getTitle()).toEqual(null);
  });

  it("updates state when heatmapInvalid is true, or false", () => {
    myInput.selectAxisAttribute = true;
    myInput.xAxis.value = [1, 2];
    myInput.yAxis.value = [4, 5, 6];
    myInput.attribute.value = [
      [10, 20],
      [30, 40, 50]
    ];
    element = getReactElement("run", myInput);
    let instance = shallow(element).instance();
    jest.spyOn(instance, 'setState');
    instance.getData();
    expect(instance.setState).toHaveBeenCalledWith({ heatmapInvalid: true });
    
    myInput.attribute.value = null;
    element = getReactElement("run", myInput);
    instance = shallow(element).instance();
    jest.spyOn(instance, 'setState');
    instance.getData();
    expect(instance.setState).toHaveBeenCalledWith({ heatmapInvalid: false });
  });

  it("updates zmin and zmax when fixedScale is true", () => {
    myInput.fixedScale = true;
    myInput.minValue = 10;
    myInput.maxValue = 20;
    element = getReactElement("run", myInput);
    let expectedData = [{
      z: [[30, 60, 1], [20, 1, 60], [1, 20, 30]], x: null, y: null, type: "heatmap", colorscale: "Jet", zmax: 20, zmin: 10
    }];
    expect(shallow(element).instance().getData()).toEqual(expectedData);

    myInput.minValue = 20;
    myInput.maxValue = 10;
    element = getReactElement("run", myInput);
    expectedData[0].zmax = 30;
    expectedData[0].zmin = 1;
    expect(shallow(element).instance().getData()).toEqual(expectedData);
  });

  it("does not update zmin and zmax when fixedScale is false", () => {
    myInput.fixedScale = false;
    myInput.minValue = 10;
    myInput.maxValue = 20;
    element = getReactElement("run", myInput);
    let expectedData = [{
      z: [[30, 60, 1], [20, 1, 60], [1, 20, 30]], x: null, y: null, type: "heatmap", colorscale: "Jet"
    }];
    expect(shallow(element).instance().getData()).toEqual(expectedData);
  });

  it("sets display to the right value when showAttribute = 'Label'", () => {
    myInput.showAttribute = "Label";
    myInput.attribute.label = "HeatMap Label";
    element = getReactElement("run", myInput);
    let expectedData = "sys/tg_test/Input/HeatMap Label";
    expect(shallow(element).instance().getLayout().title).toEqual(expectedData);

    myInput.attribute.label = "";
    element = getReactElement("run", myInput);
    expectedData = "sys/tg_test/Input/attributeLabel";
    expect(shallow(element).instance().getLayout().title).toEqual(expectedData);
  });

  it("sets display to the right value when showAttribute = 'Name'", () => {
    myInput.showAttribute = "Name";
    myInput.attribute.attribute = "HeatMap Name";
    element = getReactElement("run", myInput);
    let expectedData = "sys/tg_test/Input/HeatMap Name";
    expect(shallow(element).instance().getLayout().title).toEqual(expectedData);

    myInput.attribute.attribute = null;
    element = getReactElement("run", myInput);
    expectedData = "sys/tg_test/Input/attributeName";
    expect(shallow(element).instance().getLayout().title).toEqual(expectedData);
  });

  it("checks componentDidMount and setState have been called", () => {
    let time = date.getTime();
    process.env.REACT_APP_REFRESHING_RATE = "123"
    element = getReactElement("run", myInput);
    const instance = shallow(element).instance();
    jest.spyOn(instance, 'componentDidMount').mockImplementation(() => { instance.setState({ time: time }); });
    jest.spyOn(instance, 'setState');
    instance.componentDidMount();
    expect(instance.componentDidMount).toHaveBeenCalledTimes(1);
    expect(instance.setState).toHaveBeenCalledTimes(1);
    expect(instance.setState).toHaveBeenCalledWith({ "time": time });
  });

  it("checks componentWillUnmount clearInterval have been called", () => {
    element = getReactElement("run", myInput);
    const instance = shallow(element).instance();
    instance.interval = setInterval(() => { }, 1000);
    jest.spyOn(window, 'clearInterval');
    instance.componentWillUnmount();
    expect(clearInterval).toHaveBeenCalledWith(instance.interval);
  });

  it("checks shouldComponentUpdate returns the correct update value", () => {
    let time = date.getTime();
    element = getReactElement("run", myInput);
    const instance = shallow(element).instance();
    instance.rendered = true;
    instance.interval = true;
    instance.state.time = time;
    const nextState = { time: time + 1000 };
    expect(instance.shouldComponentUpdate(null, nextState)).toEqual(true);
    expect(instance.rendered).toBe(false);

    instance.rendered = true;
    nextState.time = time;
    expect(instance.shouldComponentUpdate(null, nextState)).toEqual(false);

    instance.interval = false;
    nextState.time = time + 1000;
    expect(instance.shouldComponentUpdate(null, nextState)).toEqual(true);
    expect(instance.rendered).toBe(false);

    instance.rendered = true;
    instance.rendered = false;
    expect(instance.shouldComponentUpdate(null, nextState)).toEqual(false);
  });
});
