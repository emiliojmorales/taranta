import React, { Component, CSSProperties } from "react";

import { WidgetProps } from "./types";
import {
  WidgetDefinition,
  BooleanInputDefinition,
  AttributeInputDefinition,
  NumberInputDefinition
} from "../types";

// prettier-ignore
const sampleData = [[2 , 3, 4, 5, 3], [6, 7, 8, 9, 2], [3, 8, 9, 10, 5], [2, 4, 7, 23, 6], [1, 6, 89, 32, 8]];

type Inputs = {
  showDevice: BooleanInputDefinition;
  showAttribute: BooleanInputDefinition;
  attribute: AttributeInputDefinition;
  showIndex: BooleanInputDefinition;
  showLabel: BooleanInputDefinition;
  fontSize: NumberInputDefinition;
  rowLimit: NumberInputDefinition;
  columnLimit: NumberInputDefinition;
  showWarning: BooleanInputDefinition;
};

type Props = WidgetProps<Inputs>;

function Table(props:Props) {
    const { mode, inputs } = props;
    const { attribute, showIndex, showLabel, showDevice,
      showAttribute, fontSize, rowLimit, columnLimit } = inputs;
    let { showWarning } = inputs;

    let value = mode === "run" ? attribute.value : mode === "library" || mode === "edit" ? sampleData : [];
    value = value === undefined || value === null ? null : value;

    const tdStyle: CSSProperties = { marginLeft: "5px", padding: "0.1em", whiteSpace: "nowrap", border: "1px solid black", textAlign: "center" };
    const mainDivStyle: CSSProperties = { marginLeft: "5px", fontSize: fontSize+"px"};
    const spanStyle: CSSProperties = { marginLeft: "5px", display: "inline"};

    let spanText = showDevice === true ? attribute.device+"/" : "";
    spanText += showAttribute === true ? attribute.attribute+":" : "";

    let allRowsCount: number = 0;
    let allColumnsCount: number = 0;
    let displayWarningMsg: boolean = false;

    if (value && value[0]) {
      allRowsCount = value.length;
      allColumnsCount = value[0].length;
    }

    if (mode === "run" && value !== null && value.length > 0) {
      if (columnLimit) {
        value.map((row: any[]) => (
          row.splice(columnLimit)
        ));
      }
      if (rowLimit) {
        value.splice(rowLimit);
      }

      displayWarningMsg = showWarning && value && value[0] &&
        (allRowsCount > value.length || allColumnsCount > value[0].length);
    }

    return (
        <div style={mainDivStyle}>
            {
              showDevice || showAttribute ?
                  <span style={spanStyle}>{spanText}</span>
              : null
            }
            {
              displayWarningMsg ? (
                <div>
                  Display {value[0].length} columns out of {allColumnsCount} and {value.length} rows out of {allRowsCount}
                </div>
              ) : null
            }
            {
                value !== null && value.length > 0 ? (
                    <table>
                        <tbody>
                            {showLabel || showIndex ? (
                                <tr>
                                    {showLabel ? <th key={Math.random()}style={tdStyle}>Index (x/y)</th> : null}
                                    {showIndex ? [
                                            showLabel ? null : <th key={Math.random()}></th>,
                                            value[0].map((_: any, i: number) => [<th style={tdStyle} key={i}>{i}</th>])
                                        ] : null}
                                </tr>
                            ) : null}
                            {value.map((row: any[], rowIndex: number) => [
                                <tr key={rowIndex}>
                                    {showIndex ? 
                                        <th style={tdStyle}>{rowIndex}</th> 
                                    : null}
                                    {showLabel && !showIndex ? (<td></td>) : null }
                                    {row.map((item: number | string, colIndex: number) => ([
                                        <td style={tdStyle} key={colIndex}>{item}</td> 
                                    ]))
                                    }
                                </tr>]
                            )}
                        </tbody>
                    </table>
                ) : null
            }
        </div>
    );
}

class ImageTable extends Component<Props> {
  public render() {
    return (
      <Table {...this.props} />
    );
  }
}

const definition: WidgetDefinition<Inputs> = {
  type: "IMAGE_TABLE",
  name: "ImageTable",
  defaultWidth: 10,
  defaultHeight: 10,
  inputs: {
    attribute: {
      label: "",
      type: "attribute",
      dataFormat: "image",
      dataType: "numeric",
      required: true
    },
    rowLimit: {
      type: "number",
      label: "Limit rows to display (0 - no limit)",
      default: 5
    },
    columnLimit: {
      type: "number",
      label: "Limit columns to display (0 - no limit)",
      default: 5
    },
    showDevice: {
      type: "boolean",
      label: "Show Device",
      default: false
    },
    showAttribute: {
      type: "boolean",
      label: "Show Attribute",
      default: false
    },
    showIndex: {
      type: "boolean",
      label: "Show Index",
      default: false
    },
    showLabel: {
      type: "boolean",
      label: "Show Labels",
      default: false
    },
    showWarning: {
      type: "boolean",
      label: "Show Warning",
      default: true
    },
    fontSize: {
      type: "number",
      label: "Font Size (px)",
      default: 16,
      nonNegative: true
    },
  }
};

export default { component: ImageTable, definition };
