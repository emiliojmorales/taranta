import React from "react";
import { AttributeInput, DeviceInput } from "../../types";
import { configure, shallow, mount, render } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import MacroButton from "./MacroButton";

interface Inputs {
  door: DeviceInput;
  macroserver: DeviceInput;
  pool: DeviceInput;
  state: AttributeInput;
  macrolist: AttributeInput;
}

configure({ adapter: new Adapter() });

describe("AttributeWriterDropdown", () => {
  const date = new Date();
  const timestamp = date.getTime();
  const writeArray: any = [];

  const initialDoorInput: DeviceInput = {
    alias: "",
    name: "door/test/1"
  };

  const initialMacroServerInput: DeviceInput = {
    alias: "",
    name: "macroserver/test/1"
  };

  const initialPoolDeviceInput: DeviceInput = {
    alias: "",
    name: "pool/test/1"
  };

  const initialStateInput: AttributeInput = {
    device: "door/test/1",
    attribute: "state",
    dataFormat: "scalar",
    dataType: "DevState",
    enumlabels: [],
    history: [],
    isNumeric: false,
    timestamp: timestamp,
    unit: "",
    write: () => {},
    value: "ON",
    writeValue: null,
    label: ""
  };

  const initialMacroListInput: AttributeInput = {
    device: "macroserver/test/1",
    attribute: "macrolist",
    dataFormat: "spectrum",
    dataType: "",
    enumlabels: [],
    history: [],
    isNumeric: false,
    timestamp: timestamp,
    unit: "",
    write: writeArray,
    value: ["macros", "ascan"],
    label: "",
    writeValue: "" //should it be "" or [] ?
  };

  const initialInputs: Inputs = {
    door: initialDoorInput,
    macrolist: initialMacroListInput,
    macroserver: initialMacroServerInput,
    pool: initialPoolDeviceInput,
    state: initialStateInput
  };

  it("renders an empty widget in edit/run mode without crashing", () => {
    const testInput = initialInputs;
    const props = {
      t0: 1,
      actualWidth: 32,
      actualHeight: 12,
      inputs: testInput
    };
    const elementInEdit = React.createElement(MacroButton.component, {
      mode: "edit",
      ...props
    });

    const elementInEditHtml = shallow(elementInEdit).html();
    expect(elementInEditHtml).toContain("door/test/1");
    expect(elementInEditHtml).toContain("Door output will be displayed here");

    const elementInRun = React.createElement(MacroButton.component, {
      mode: "run",
      ...props
    });
    const content = mount(elementInRun);
    const elementInRunHtml = shallow(elementInRun).html();
    expect(elementInRunHtml).toContain("Door output will be displayed here");
    expect(elementInRunHtml).toContain("Run");
    expect(elementInRunHtml).toContain("ON");
    expect(elementInRunHtml).toContain("Select Macro");
    expect(content.find(".macro-list-dropdown").length).toEqual(2);
    expect(elementInRunHtml).toContain("Door output will be displayed here");
  }); //renders an empty widget in edit/run mode without crashing
});
