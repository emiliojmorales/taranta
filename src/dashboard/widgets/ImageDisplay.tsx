import React, { Component, CSSProperties } from "react";

import { WidgetProps } from "./types";
import {
  WidgetDefinition,
  BooleanInputDefinition,
  AttributeInputDefinition,
  SelectInputDefinition,
  StringInputDefinition,
  NumberInputDefinition,
  ColorInputDefinition,
} from "../types";

export interface AttributeComplexInput {
  attribute: AttributeInputDefinition;
}

type Inputs = {
  attribute: AttributeInputDefinition;
  showAttribute: SelectInputDefinition;
  showDevice: BooleanInputDefinition;
  scale: StringInputDefinition;
  textColor: ColorInputDefinition;
  backgroundColor: ColorInputDefinition;
  size: NumberInputDefinition;
  font: SelectInputDefinition;
};

type Props = WidgetProps<Inputs>;

const imgSample =
  "iVBORw0KGgoAAAANSUhEUgAAAPsAAAD7CAAAAACJGb16AAAC40lEQVR4nO3b227jMAxFUbLo//8y+6Ab5Sh902GR7jUdFM1gEB+TlBM5NQMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA4H/x4icM9fMn39Jn89PJLouvzO7n7GXpdT0/km/PGP2rJL2s7n7MPlWEF2X3+ec08B5ekV6T3VvZ3w68mYXLw0uyt+C/ZQ83feUV2d1n+hY/Cx/JXR1ekL1HTyO/m8nV4e9n90f6HD58fg+3MO3MS+ru7e+6zu3aGdCv9dezu/uIf2r6dn3rwcXpb2cfybeRf2r93tILwwvmPRf+lL0Nu77st7O7u+X4/TrXzevbDC6NL6l77vu98P01zSj7J9XdfXzfF7zs0e/Cib+/1rW23xe8ZS1zfRY+aq2z0fZpwRvmMqdveLuc3VNMzwteeygtcyu3cLWT9Pxj7FPTb4PeH5FR7V3ksZ/dEEWD3kn3Kn0r/FjmyvYqtXvUthW+ZqdqkWe3tARWBrea7CZ+7frOV9Hzyu+FHVRlr696Wc//hegl2XvwqD4B8uxhY0cyLGrjK7OHWXuPOsKHWUTdCRBl77Fz2VPhi07A7ew9ko8f8tZEWCt8/ynUp+Bq9sh7czaDr8LH7Pv1f3TxJT3f36u1fvet8KPvK7pe0vNj0J93ncJm4c30Y383ezwG/XG/LXr6feyvHlF2v+fnoLdpz/GjjbzN+B91P27ebkjL3D7wacEz6Uonqru/LnPj3yIteB9W93F78c3N9VjpR/zLx5Ndr/u2zPnjE0UxrnMlhRdc3+eIvxY+8sjLr/CCee/9PpK/XuHTgnf9aDJJ3UfyU+HHda6nV1K8pk3Jn+lT0+v3MnSv54/JIl/nFIeSad6/v/3AaKSRlxxJJtsrPn+OOtZ1Tk+2Z3UON7OrDiNT3iM4fbKwLnnBXuXrY2V7tf/596QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD+gh9FZWLIxuSZwgAAAABJRU5ErkJggg==";

class ImageDisplay extends Component<Props> {
  public constructor(props: Props) {
    super(props);
    this.GenerateImage = this.GenerateImage.bind(this);
  }

  public render() {
    const { mode, inputs } = this.props;
    const {
      attribute,
      showAttribute,
      showDevice,
      scale,
      textColor,
      size,
      font,
      backgroundColor,
    } = inputs;
    const device = attribute.device || "device";
    let display = "";
    const userScale = (parseFloat(scale) * 100).toString() + "%";
    if (showAttribute === "Label") display = attribute.label;
    else if (showAttribute === "Name") display = attribute.attribute;

    const style: CSSProperties = {
      padding: "0.5em",
      whiteSpace: "pre-wrap",
      color: textColor,
      fontSize: size + "em",
    };
    if (font) {
      style["fontFamily"] = font;
    }

    if (mode === "run") {
      const content = null ? "Loading" : this.GenerateImage(attribute.value);
      return (
        <div style={{ width: "100%", height: "100%", backgroundColor: backgroundColor}} >
          <div id="ImageDisplay" style={style}>
            {showDevice ? device : ""}
            {showDevice && showAttribute && "/"}
            {display}
            {(showDevice || showAttribute !== "None") && ": "}
            <img
              src={content}
              alt="Failed at fetching the device attribute"
              width={userScale}
              height={userScale}
            />
          </div>
        </div>
      );
    } else {
      return (
        <div style={{ width: "100%", height: "100%", backgroundColor: backgroundColor}} >
          <div id="ImageDisplay" style={style}>
            {showDevice ? device : ""}
            {showDevice && showAttribute && "/"}
            {display}
            {(showDevice || showAttribute !== "None") && ": "}
            <img
              src={"data:image/png;base64," + imgSample}
              alt="Failed at fetching the device attribute"
            />
          </div>
        </div>
      );
    }
  }

  private GenerateImage(image) {
    if (image === undefined || image === null) {
      return "";
    }
    const canvas = document.createElement("canvas");
    const height = image.length;
    const width = image[0].length;

    let max = Number(image[0][0]);
    for (let x = 0; x < width; x++) {
      for (let y = 0; y < height; y++) {
        const pixel = Number(image[x][y]);
        if (pixel > max) {
          max = pixel;
        }
      }
    }

    const context = canvas.getContext("2d");
    if (context == null) {
      return "";
    }

    const imgData = context.createImageData(width, height);

    for (let x = 0; x < width; x++) {
      for (let y = 0; y < height; y++) {
        const value = image[x][y];
        const index = y * width * 4 + x * 4;
        const normal = 255 * (Number(value) / (max === 0 ? 1 : max));
        imgData.data[index + 0] = normal;
        imgData.data[index + 1] = normal;
        imgData.data[index + 2] = normal;
        imgData.data[index + 3] = 255;
      }
    }

    context.putImageData(imgData, 0, 0);
    var img = new Image();
    img.src = canvas.toDataURL("image/png");
    return img.src;
  }
}

const definition: WidgetDefinition<Inputs> = {
  type: "Image_Display",
  name: "Image Display",
  historyLimit: 1,
  defaultWidth: 30,
  defaultHeight: 20,
  inputs: {
    attribute: {
      label: "",
      type: "attribute",
      dataFormat: "image",
      required: true,
    },
    showAttribute: {
      type: "select",
      label: "Attribute display:",
      default: "Label",
      options: [
        {
          name: "Label",
          value: "Label",
        },
        {
          name: "Name",
          value: "Name",
        },
        {
          name: "None",
          value: "None",
        },
      ],
    },
    showDevice: {
      type: "boolean",
      label: "Device Name",
      default: false,
    },
    scale: {
      type: "string",
      default: "1",
    },
    textColor: {
      label: "Text Color",
      type: "color",
      default: "#000000",
    },
    backgroundColor: {
      label: "Background Color",
      type: "color",
      default: "#ffffff",
    },
    size: {
      label: "Text size (in units)",
      type: "number",
      default: 1,
      nonNegative: true,
    },
    font: {
      type: "select",
      default: "Helvetica",
      label: "Font type",
      options: [
        {
          name: "Default (Helvetica)",
          value: "Helvetica",
        },
        {
          name: "Monospaced (Courier new)",
          value: "Courier new",
        },
      ],
    },
  },
};

export default { component: ImageDisplay, definition };
