import React from "react";
import { AttributeInput } from "../types";
import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import SimpleMotor from "./SimpleMotor";

interface Input {
  device: string;
  state: string;
  status: string;
  doubleScalar: AttributeInput;
  ulongScalar: AttributeInput;
}

configure({ adapter: new Adapter() });

describe("SimpleMotor", () => {
  let doubleScalarAttribute: AttributeInput,
    ulongScalarAttribute: AttributeInput;
  let myInput: Input;

  it("renders run mode true without issues", () => {
    doubleScalarAttribute = {
      value: 1.2134
    }

    ulongScalarAttribute = {
      value: 0.123
    }

    myInput = {
      doubleScalar: doubleScalarAttribute,
      ulongScalar: ulongScalarAttribute
    };

    const element = React.createElement(SimpleMotor.component, {
      mode: "run",
      t0: 1,
      inputs: myInput
    });

    expect(shallow(element).html()).toContain(`ULong Scalar: ${ulongScalarAttribute.value}`);
    expect(shallow(element).html()).toContain(`Double Scalar: ${doubleScalarAttribute.value}`);
  });

});