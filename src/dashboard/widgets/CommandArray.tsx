import React, { Component, CSSProperties } from "react";
import { WidgetProps } from "./types";
import {
  WidgetDefinition,
  NumberInputDefinition,
  BooleanInputDefinition,
  CommandInputDefinition,
  StringInputDefinition,
  ColorInputDefinition,
  SelectInputDefinition,
  ComplexInputDefinition,
  StyleInputDefinition
} from "../types";
import { Button } from "react-bootstrap";

import Modal from "../../shared/modal/components/Modal/Modal";

import OutputDisplay from "../../shared/utils/OutputDisplay";

import InputField from "../../shared/utils/InputField";
import { parseCss } from "../components/Inspector/StyleSelector";

type Inputs = {
  command: CommandInputDefinition;
  commandArgs: ComplexInputDefinition;
  showDevice: BooleanInputDefinition;
  showCommand: BooleanInputDefinition;
  title: StringInputDefinition;
  buttonText: StringInputDefinition;
  requireConfirmation: BooleanInputDefinition;
  displayOutput: BooleanInputDefinition;
  placeholder: SelectInputDefinition;
  OutputMaxSize: NumberInputDefinition;
  timeDisplayOutput: NumberInputDefinition;
  textColor: ColorInputDefinition;
  backgroundColor: ColorInputDefinition;
  size: NumberInputDefinition;
  font: SelectInputDefinition;
  btnCss: StyleInputDefinition;
  widgetCss: StyleInputDefinition;
};

type Props = WidgetProps<Inputs>;

interface State {
  input: string;
  pending: boolean;
  showModal: boolean;
}

let waiting = false,
  displayOutputCommand = 0;

class CommandArray extends Component<Props, State> {
  public constructor(props: Props) {
    super(props);
    this.state = {
      input: "",
      pending: false,
      showModal: false,
    };
    this.handleExecute = this.handleExecute.bind(this);
  }

  public render() {
    const { inputs, id } = this.props;
    const {
      command,
      showDevice,
      showCommand,
      backgroundColor,
      textColor,
      size,
      font,
      timeDisplayOutput,
      OutputMaxSize,
      placeholder
    } = inputs;

    const { device, command: commandName } = command;
    const acceptedType: string = command["acceptedType"]
      ? command["acceptedType"]
      : "DevString";

    const intypedesc = command["intypedesc"];
    const deviceLabel = device || "device";
    const commandLabel = commandName || "command";

    const label = [
      ...(showDevice ? [deviceLabel] : []),
      ...(showCommand ? [commandLabel] : []),
    ].join("/");

    let output = command.output;

    if (
      output[0] !== undefined &&
      displayOutputCommand === id &&
      !waiting &&
      timeDisplayOutput !== 0
    ) {
      waiting = true;
      setTimeout(() => {
        displayOutputCommand = 0;
        waiting = false;
        this.setState({});
      }, timeDisplayOutput);
    }

    const widgetCss = this.props.inputs.widgetCss ? parseCss(this.props.inputs.widgetCss).data : {}
    const btnCss = parseCss(inputs.btnCss).data;

    const style: CSSProperties = {
      display: "flex",
      alignItems: "center",
      padding: "0.25em 0.5em",
      backgroundColor,
      color: textColor,
      fontSize: size + "em",
      height: "100%",
      ...widgetCss
    };
    if (font) {
      style["fontFamily"] = font;
    }

    const buttonLabel = this.props.inputs.buttonText || "Submit";
    const titleLabel = this.props.inputs.title;
    let outputSpan = <div />;

    const outputAll = output;
    let viewMore = <div />;
    if (
      output.toString().length > OutputMaxSize &&
      displayOutputCommand === id
    ) {
      output = output.toString().substr(0, OutputMaxSize) + "...";
      viewMore = (
        <div
          style={{
            fontSize: "10px",
            float: "left",
            cursor: "pointer",
            textDecoration: "underline",
            color: "blue",
          }}
          id="viewmore"
          onClick={() => this.openModal()}
        >
          View More
        </div>
      );
    }

    if (displayOutputCommand === id && this.props.inputs.displayOutput) {
      outputSpan = (
        <div
          style={{
            maxHeight: this.props.actualHeight + "px",
            paddingLeft: "3px",
          }}
          title={outputAll}
        >
          <OutputDisplay value={output} isLoading={false} />
        </div>
      );
    }

    const modal = (
      <Modal title={device + "/" + commandName} transparentModal={true}>
        <Modal.Body>
          <OutputDisplay value={outputAll} isLoading={false} />
        </Modal.Body>
        <Modal.Footer>
          <Button
            id="btn-close"
            variant="primary"
            onClick={() => this.closeModal()}
          >
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    );

    const renderType = this.props.inputs.commandArgs && this.props.inputs.commandArgs.length > 0 ? 'custom' : acceptedType;

    return (
      <div style={style} className="">
        <table className="separated w-100">
          <tbody>
            <tr>
              <td>
                {titleLabel && (
                  <span style={{ flexGrow: 0, marginRight: "0.5em" }}>
                    {titleLabel}
                  </span>
                )}
                {label && (
                  <span style={{ flexGrow: 0, marginRight: "0.5em" }}>
                    {label}:
                  </span>
                )}
              </td>
              <td>
                <InputField
                  isEnabled={true}
                  onExecute={this.handleExecute}
                  commandArgs={this.props.inputs.commandArgs}
                  intype={acceptedType}
                  renderType={renderType}
                  intypedesc={intypedesc}
                  buttonLabel={buttonLabel}
                  placeholder={placeholder}
                  btnCss={btnCss}
                />
              </td>
              <td>
                {outputSpan}
                {viewMore}
              </td>
            </tr>
          </tbody>
        </table>
        <div id="modal-wrapper">
          {this.state.showModal && modal}
        </div>
      </div>
    );
  }

  private async handleExecute(commandv, value) {
    if (this.state.pending) {
      return;
    }

    const { command, requireConfirmation } = this.props.inputs;

    let acceptedType = this.props.inputs.command["dataType"];
    if (!acceptedType) acceptedType = this.props.inputs.command["acceptedType"];

    const parameterMessage = ` with parameter ` + value;
    let message = `Confirm executing ${command.command} on ${command.device}`;
    if (acceptedType)
      message += acceptedType.includes("Void") ? "" : parameterMessage;

    /* eslint-disable no-restricted-globals */
    if (!requireConfirmation || confirm(message)) {
      this.setState({ input: "", pending: true });

      displayOutputCommand = this.props.id;
      command.execute(value);

      this.setState({ input: "", pending: false });
    }
  }

  openModal() {
    this.setState({ showModal: true });
  }

  closeModal() {
    this.setState({ showModal: false });
  }
}

const definition: WidgetDefinition<Inputs> = {
  type: "COMMAND_ARRAY",
  name: " Command",
  defaultHeight: 3,
  defaultWidth: 20,
  inputs: {
    title: {
      type: "string",
      label: "Title",
    },
    buttonText: {
      type: "string",
      label: "Button Label",
    },
    command: {
      label: "",
      type: "command",
      required: true,
      intype: "Any",
    },

    commandArgs: {
      label: "Predefined Command Args",
      type: "complex",
      repeat: true,
      inputs: {
        name: {
          type: "string",
          label: "Name",
          default: ""
        },
        value: {
          type: "string",
          label: "Value",
          default: ""
        },
        isDefault: {
          type: "radio",
          label: "Make default",
          default: false
        }
      }
    },

    showDevice: {
      type: "boolean",
      label: "Show Device Name",
      default: true,
    },
    showCommand: {
      type: "boolean",
      label: "Show Commnad Name",
      default: true,
    },
    requireConfirmation: {
      type: "boolean",
      label: "Require Confirmation",
      default: true,
    },
    displayOutput: {
      type: "boolean",
      label: "Display Output",
      default: true,
    },
    placeholder: {
      type: "select",
      default: "intype",
      label: "Select placeholder display",
      options: [
        {
          name: "Display In Type",
          value: "intype",
        },
        {
          name: "Display In Type Description",
          value: "intypedesc",
        },
      ],
    },
    OutputMaxSize: {
      type: "number",
      label: "Maximum size for output display",
      default: 20,
      nonNegative: true,
    },
    timeDisplayOutput: {
      type: "number",
      label: "Time out for output display ms",
      default: 3000,
      nonNegative: true,
    },
    textColor: {
      label: "Text Color",
      type: "color",
      default: "#000000",
    },
    backgroundColor: {
      label: "Background Color",
      type: "color",
      default: "#ffffff",
    },
    size: {
      label: "Text size (in units)",
      type: "number",
      default: 1,
      nonNegative: true,
    },
    font: {
      type: "select",
      default: "Helvetica",
      label: "Font type",
      options: [
        {
          name: "Default (Helvetica)",
          value: "Helvetica",
        },
        {
          name: "Monospaced (Courier new)",
          value: "Courier new",
        },
      ],
    },
    btnCss: {
      type: "style",
      label: "Submit button CSS",
      default: ""
    },
    widgetCss: {
      type: "style",
      label: "Widget CSS",
      default: ""
    }
  },
};

export default {
  definition,
  component: CommandArray,
};
