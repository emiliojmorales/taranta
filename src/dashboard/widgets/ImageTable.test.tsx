import React from "react";
import { AttributeInput } from "../types";

import { configure, shallow, mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import ImageTable from "./ImageTable";

interface Input {
  showDevice: boolean;
  showAttribute: boolean;
  attribute: AttributeInput;
  showIndex: boolean;
  showLabel: boolean;
  fontSize: number;
}

configure({ adapter: new Adapter() });

describe("ImageTable", () => {
  let myAttributeInput: AttributeInput;
  let myInput: Input;
  var writeArray: any = [];
  var date = new Date();
  var timestamp = date.getTime();

  it("renders without crashing", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "double_image",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      write: writeArray,
      value: [[1], [2], [3], [4]],
      writeValue: "",
      timestamp: timestamp
    };

    myInput = {
        showDevice: true,
        showAttribute: true,
        attribute: myAttributeInput,
        showIndex: false,
        showLabel: false,
        fontSize: 12,
    };

    const element = React.createElement(ImageTable.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput
    });
    expect((shallow(element).html().match(/<th/g) || []).length).toEqual(0);
    expect((shallow(element).html().match(/<td/g) || []).length).toEqual(4);
    expect((shallow(element).html().match(/<tr/g) || []).length).toEqual(4);
  });

  it("renders without crashing for more dimensions", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "double_image",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      write: writeArray,
      value: [[1, 1, 1], [2, 2, 2], [3, 3, 3], [4, 4, 4]],
      writeValue: "",
      timestamp: timestamp
    };

    myInput = {
        showDevice: true,
        showAttribute: true,
        attribute: myAttributeInput,
        showIndex: false,
        showLabel: false,
        fontSize: 12,
    };

    const element = React.createElement(ImageTable.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput
    });
    expect((shallow(element).html().match(/<th/g) || []).length).toEqual(0);
    expect((shallow(element).html().match(/<td/g) || []).length).toEqual(12);
    expect((shallow(element).html().match(/<tr/g) || []).length).toEqual(4);
  });

  it("renders without crashing for more dimensions and index visible", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "double_image",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      write: writeArray,
      value: [[1, 1, 1], [2, 2, 2], [3, 3, 3], [4, 4, 4]],
      writeValue: "",
      timestamp: timestamp
    };

    myInput = {
        showDevice: true,
        showAttribute: true,
        attribute: myAttributeInput,
        showIndex: true,
        showLabel: false,
        fontSize: 12,
    };

    const element = React.createElement(ImageTable.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput
    });
    expect((shallow(element).html().match(/<th/g) || []).length).toEqual(8);
    expect((shallow(element).html().match(/<td/g) || []).length).toEqual(12);
    expect((shallow(element).html().match(/<tr/g) || []).length).toEqual(5);
  });

  it("renders without crashing for more dimensions and index  and label visible", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "double_image",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      write: writeArray,
      value: [[1, 1, 1], [2, 2, 2], [3, 3, 3], [4, 4, 4]],
      writeValue: "",
      timestamp: timestamp
    };

    myInput = {
        showDevice: true,
        showAttribute: true,
        attribute: myAttributeInput,
        showIndex: true,
        showLabel: true,
        fontSize: 12,
    };

    const element = React.createElement(ImageTable.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput
    });
    expect((shallow(element).html().match(/<th/g) || []).length).toEqual(8);
    expect((shallow(element).html().match(/<td/g) || []).length).toEqual(12);
    expect((shallow(element).html().match(/<tr/g) || []).length).toEqual(5);
  });

});