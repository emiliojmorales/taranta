import React, { CSSProperties } from "react";
import { WidgetProps } from "./types";
import "./styles/Tabular.css"

import {
  WidgetDefinition,
  DeviceInputDefinition,
  BooleanInputDefinition,
  ComplexInputDefinition,
  ColorInputDefinition,
  SelectInputDefinition,
  StyleInputDefinition,
  NumberInputDefinition,
  AttributeInputDefinition
} from "../types";

import { parseCss } from "../components/Inspector/StyleSelector";
import config from '../../config.json';

export interface AttributeComplexInput {
  attribute: AttributeInputDefinition;
}

export interface DeviceComplexInput {
  device: DeviceInputDefinition;
}

type Inputs = {
  devices: ComplexInputDefinition<DeviceComplexInput>;
  customAttributes: ComplexInputDefinition<AttributeComplexInput>;
  showDefaultAttribute: BooleanInputDefinition;
  compactTable: BooleanInputDefinition;
  borderedTable: BooleanInputDefinition;
  textColor: ColorInputDefinition;
  backgroundColor: ColorInputDefinition;
  size: NumberInputDefinition;
  font: SelectInputDefinition;
  widgetCss: StyleInputDefinition
};

type Props = WidgetProps<Inputs>;

function Tabular(props: Props) {
  const {
    devices,
    customAttributes,
    compactTable,
    borderedTable,
    textColor,
    backgroundColor,
    font,
    size
  } = props.inputs;

  const widgetCss = props.inputs.widgetCss ? parseCss(props.inputs.widgetCss).data : {};
  const headerData = getTableHeader(customAttributes);
  const style: CSSProperties = {
    padding: "0.5em",
    display: "flex",
    width: "100%",
    overflow: "auto",
    whiteSpace: "pre-wrap",
    backgroundColor,
    color: textColor,
    fontSize: size + "em",
    ...widgetCss
  };
  if (font) {
    style["fontFamily"] = font;
  }


  return (
    <div>
      {props.mode !== "library" &&
      <div id="TabularView" style={style} className={`table-responsive ${compactTable === true ? "table-responsive table-sm " : ""} `}>
        {devices.length > 0 &&
          <table className={`table ${borderedTable === true ? "table-bordered " : ""} table-hover align-items-center justify-content-center"`}>
            <tbody>
              <tr className="thead-light">
                <th>Device</th>
                {headerData.map((header, i) => {
                  return (
                    <th key={i}>{header}</th>
                  )
                })
                }
              </tr>
              {devices.map((device, i) => {
                return (
                  <tr key={i}>
                    <td key={i}><b>{device.device.name}</b></td>
                    {customAttributes.map((obj, j) => {
                      let data;
                      if (obj.attribute && undefined !== obj.attribute?.value) {
                        data = obj.attribute;
                      } else if (obj.attribute && obj.attribute[i] && undefined !== obj.attribute[i].value) {
                          data = obj.attribute[i];
                      }
                      return (
                        <td key={j}>
                          <span className={` ${data?.dataType === "DevState" ? "stateTango " + data.value : ""}`}>
                            {getFormattedValue(data)}
                          </span>
                        </td>
                      )
                    })
                    }
                  </tr>
                )
              })
              }
            </tbody>
          </table>
        }
        {devices.length === 0 &&
          "No devices configured"
        }
        </div>
      }
      {props.mode === "library" &&
        <div className="table-responsive table-responsive table-sm" >
          <table className="table table-hover align-items-center justify-content-center">
          <tbody>
              <tr className="thead-light">
                <th>Device</th>
                <th>Attribute 1</th>
                <th>Attribute 2</th>
              </tr>
              <tr>
                <td><b>device/test/1</b></td>
                <td>2.0</td>
                <td className="tangoState ON">ON</td>
              </tr>
              <tr>
                <td><b>device/test/2</b></td>
                <td>12</td>
                <td className="tangoState RUNNING">RUNNING</td>
              </tr>
            </tbody>
          </table>
        </div>
      }
    </div>
  );

  /**
   * This function will return header for the table.
   * This makes use of default attributes for customAttributes that are missing on all configured devices
   */
  function getTableHeader(customAttributes) {
    let defaultAttr = [...config.defaultAttribute];
    const header = customAttributes.map(obj => {
      //Get the subscribed attrs from first non-empty entry of customAtt array
      if (obj.attribute && obj.attribute.length > 0) {
        for (let i = 0; i < obj.attribute.length; i++) {
          if (obj.attribute && obj.attribute[i] && obj.attribute[i].attribute) {
            // If its not empty then return attr also filter duplicate attr (one in default attr)
            defaultAttr = defaultAttr.filter(x => x.toLocaleUpperCase() !== obj.attribute[i].attribute.toLocaleUpperCase())
            return obj.attribute[i].attribute;
          }
        }
      } else if (obj.attribute && obj.attribute.attribute) {
        defaultAttr = defaultAttr.filter(x => x.toLocaleUpperCase() !== obj.attribute.attribute.toLocaleUpperCase())
        return obj.attribute.attribute;
      }

      const val = 'run' === props.mode && defaultAttr.length > 0 ? defaultAttr.shift() : '-';
      return (val ? val : '-');
    })

    return header;
  }

  function getFormattedValue(obj: any, precision = 2) {

    let value = obj && obj.value ? obj.value : '-';
    if (obj?.dataType === "DevEnum")
      value = obj.enumlabels[obj.value]

    return (isNaN(value) ? value : value?.toFixed(precision));
  }
}

const definition: WidgetDefinition<Inputs> = {
  type: "TABULAR_VIEW",
  name: "Tabular view",
  defaultWidth: 20,
  defaultHeight: 10,
  inputs: {
    devices: {
      type: "complex",
      label: "Devices",
      repeat: true,
      inputs: {
        device: {
          type: "device",
          label: "",
          publish: "$device"
        },
      }
    },
    showDefaultAttribute: {
      type: "boolean",
      label: "Show default attributes",
      default: true
    },
    customAttributes: {
      type: "complex",
      label: "Custom Attributes",
      repeat: true,
      inputs: {
        attribute: {
          label: "Attribute",
          type: "attribute",
          required: false
        },
      }
    },
    compactTable: {
      type: "boolean",
      label: "Show compact table",
      default: true
    },
    borderedTable: {
      type: "boolean",
      label: "Show bordered table",
      default: false
    },
    textColor: {
      label: "Text Color",
      type: "color",
      default: "#000000"
    },
    backgroundColor: {
      label: "Background Color",
      type: "color",
      default: "#ffffff"
    },
    size: {
      label: "Text size (in units)",
      type: "number",
      default: 1,
      nonNegative: true
    },
    font: {
      type: "select",
      default: "Courier new",
      label: "Font type",
      options: [
        {
          name: "Default (Courier new)",
          value: "Courier new"
        },
        {
          name: "Helvetica",
          value: "Helvetica"
        }
      ]
    },
    widgetCss: {
      type: "style",
      label: "Custom CSS",
      default: ""
    }
  }
};

export default { component: Tabular, definition };
