import { createStore, applyMiddleware, compose } from "redux";
import { createLogger } from "redux-logger";
import createSagaMiddleware from "redux-saga";

import rootSaga from "./sagas";
import rootReducer from "./reducers";
import {REDUX_LOG} from "../../config.json";

const loggerMiddlware = createLogger();
const sagaMiddleware = createSagaMiddleware();

const { __REDUX_DEVTOOLS_EXTENSION_COMPOSE__ } = window;
const composeEnhancer = __REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  rootReducer,
  composeEnhancer(REDUX_LOG ? applyMiddleware(loggerMiddlware, sagaMiddleware): applyMiddleware(sagaMiddleware))
);

sagaMiddleware.run(rootSaga);
export default store;
