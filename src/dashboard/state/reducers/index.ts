import { combineReducers } from "redux";

import user, {
  IUserState as UserState
} from "../../../shared/user/state/reducer";
import ui, { UIState } from "./ui";
import selectedDashboard, {
  SelectedDashboardState
} from "./selectedDashboard/index";
import canvases, { CanvasesState } from "./canvases";
import dashboards, { DashboardsState } from "./dashboards";
import notifications, { NotificationsState } from "./notifications";
import clipboard, { ClipboardState } from "./clipboard";
import elastic, { IElasticState } from "../../../shared/elastic/state/reducer"

export interface RootState {
  ui: UIState;
  canvases: CanvasesState;
  selectedDashboard: SelectedDashboardState;
  user: UserState;
  elastic: IElasticState;
  dashboards: DashboardsState;
  notifications: NotificationsState;
  clipboard: ClipboardState;
}

export default combineReducers<RootState>({
  ui,
  canvases,
  selectedDashboard,
  user,
  elastic,
  dashboards,
  notifications,
  clipboard,
});
