import React, { Component, CSSProperties } from "react";
import Files from "react-files";
import { CommandInput } from "../../../dashboard/types";

import "./styles/command-file.styles.css";
import Editor from "react-simple-code-editor";
import { highlight, languages } from "prismjs/components/prism-core";
import "prismjs/components/prism-clike";
import "prismjs/components/prism-javascript";
import "./styles/prism.css";
import ReactDiffViewer from "react-diff-viewer";
import Draggable from "react-draggable";

type Props = {
  label: string;
  uploadBtnCss?: CSSProperties;
  uploadBtnLabel: string;
  sendBtnCss?: CSSProperties;
  sendBtnText: string;
  fullOutputStyle?: CSSProperties;
  output: string;
  mode: string;
  requireConfirmation: boolean;
  command?: CommandInput;
  commandFromDevice?: Function;
  commandName: string;
  commandDevice: string;
  outerDivCss?: CSSProperties;
};

interface State {
  fileName: String;
  fileContent: any;
  fileType: String;
  updateFileContent: any;
  pending: boolean;
  showFileViewModal: boolean;
  commandStatus: String;
  showCompare: boolean;
  restoreValue: boolean;
}

const compareStyles = {
  line: {
    fontSize: 13,
  },
};

const defaultStyle = { marginTop: "5px", marginBottom: "5px" };

const buttonStyle = {
  marginTop: "5px",
  marginBottom: "5px",
};

class CommandArgsFile extends Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      fileName: "No file selected",
      fileContent: "",
      fileType: "",
      updateFileContent: "",
      pending: false,
      showFileViewModal: false,
      commandStatus: "",
      showCompare: false,
      restoreValue: false,
    };

    this.onFilesChange = this.onFilesChange.bind(this);
    this.handleSendCommand = this.handleSendCommand.bind(this);
  }

  public render() {
    return (
      <div className="outer-wrapper">
        <div
          className="command-file-wrapper"
          style={this.props.outerDivCss || defaultStyle}
        >
          <div className="file-upload-label-container">
            <Files
              onChange={this.onFilesChange}
              onError={this.onFilesError}
              multiple={false}
              maxFileSize={25165824}
              minFileSize={0}
              clickable
            >
              <span className="widget-label">{this.props.label}</span>
              <div className="upload-div">
                <button
                  style={this.props.uploadBtnCss || defaultStyle}
                  className="btn-upload btn taranta-btn btn-secondary btn-sm"
                  title="Select file(Max size 24MB) to send along with command"
                >
                  <span className="btn-text">{this.props.uploadBtnLabel}</span>
                </button>
              </div>
            </Files>
            <div>
              <div
                title={
                  "" !== this.state.fileContent
                    ? 'Click here to view or edit the content of "' +
                      this.state.fileName.toString() +
                      '"'
                    : ""
                }
                onClick={(e) => {
                  this.openFileViewModal(e);
                }}
                className={
                  "file-name-wrapper " +
                  ("" !== this.state.fileContent ? "link-view" : "")
                }
              >
                <b>
                  {this.state.fileContent !== this.state.updateFileContent &&
                    "[Modified] "}
                </b>
                {"run" === this.props.mode &&
                  this.truncate(this.state.fileName, 16)}
              </div>
            </div>
          </div>
          <button
            style={this.props.sendBtnCss || buttonStyle}
            onClick={this.handleSendCommand}
            disabled={this.state.fileContent.length < 1 || this.state.pending}
            className="btn btn-outline-secondary btn-send btn-sm"
            title="Click this button to fire command"
          >
            <span className="btn-text">{this.props.sendBtnText}</span>
          </button>
          <span className="output-wrapper" style={this.props.fullOutputStyle}>
            {this.props.output}
          </span>
        </div>
        <div
          id="modal-file-viewer"
          className={false === this.state.showFileViewModal ? "hide" : ""}
        >
          <div>
            <div
              role="dialog"
              className={
                "modal modal-inner " +
                (true === this.state.showFileViewModal ? "show" : "")
              }
            >
              <Draggable handle=".modal-header">
                <div className="modal-dialog">
                  <div className="modal-content" role="document">
                    <div className="modal-header">
                      <h4 className="modal-title">View File Content</h4>
                    </div>
                    {this.state.fileType === "binary" && (
                      <div className="modal-body file-view-body">
                        Unable to generate a preview for a binary file.
                      </div>
                    )}
                    {this.state.fileType === "text" && (
                      <div>
                        <div className="modal-body file-view-body">
                          {!this.state.restoreValue && (
                            <button
                              onClick={() => {
                                this.setState({ restoreValue: true });
                              }}
                              disabled={
                                this.state.fileContent ===
                                this.state.updateFileContent
                              }
                              className="btn-restore taranta-btn btn btn-primary btn-sm"
                              title="Restore"
                            >
                              <span className="btn-text">Restore Content</span>
                            </button>
                          )}
                          {this.state.restoreValue && (
                            <div>
                              <span>Are you sure?</span>
                              <button
                                onClick={() => {
                                  this.setState({
                                    updateFileContent: this.state.fileContent,
                                  });
                                  this.setState({ showCompare: false });
                                  this.setState({ restoreValue: false });
                                }}
                                className="btn-yes taranta-btn btn btn-primary btn-sm"
                                title="Yes"
                              >
                                <span className="btn-text">Yes</span>
                              </button>
                              <button
                                onClick={() => {
                                  this.setState({
                                    showCompare: false,
                                    restoreValue: false,
                                  });
                                }}
                                className="btn-no taranta-btn btn btn-secondary btn-sm"
                                title="No"
                              >
                                <span className="btn-text">No</span>
                              </button>
                            </div>
                          )}
                          {!this.state.showCompare && (
                            <button
                              onClick={() => {
                                this.setState({ showCompare: true });
                              }}
                              disabled={
                                this.state.fileContent ===
                                this.state.updateFileContent
                              }
                              className="btn-compare taranta-btn btn btn-primary btn-sm"
                              title="compare"
                            >
                              <span className="btn-text">Compare</span>
                            </button>
                          )}
                          {this.state.showCompare && (
                            <button
                              onClick={() => {
                                this.setState({ showCompare: false });
                              }}
                              disabled={
                                this.state.fileContent ===
                                this.state.updateFileContent
                              }
                              className="btn-edit taranta-btn btn btn btn-primary btn-sm"
                              title="Restore"
                            >
                              <span className="btn-text">Edit</span>
                            </button>
                          )}
                          {!this.state.showCompare && (
                            <Editor
                              value={this.state.updateFileContent}
                              onValueChange={(code) =>
                                this.setState({ updateFileContent: code })
                              }
                              highlight={(code) =>
                                highlight(code, languages.js)
                              }
                              padding={10}
                              style={{
                                fontFamily:
                                  '"Fira code", "Fira Mono", monospace',
                                fontSize: 14,
                              }}
                            />
                          )}
                          {this.state.showCompare && (
                            <ReactDiffViewer
                              styles={compareStyles}
                              hideLineNumbers={true}
                              oldValue={this.state.fileContent}
                              newValue={this.state.updateFileContent}
                              splitView={true}
                            />
                          )}
                        </div>
                      </div>
                    )}
                    <div className="modal-footer">
                      <button
                        type="button"
                        className="btn-close-modal btn btn-primary"
                        onClick={(e) => {
                          this.setState({ showFileViewModal: false });
                        }}
                      >
                        Close
                      </button>
                    </div>
                  </div>
                </div>
              </Draggable>
            </div>
          </div>
        </div>
      </div>
    );
  }

  /**
   * Event Listeners
   */
  openFileViewModal(e) {
    if ("" === this.state.fileContent) return;
    this.setState({ showFileViewModal: true });
  }

  async onFilesChange(files) {
    try {
      if (files[0] !== undefined) {
        let uploadFileName = files[0].name;
        this.setState({ fileName: uploadFileName });

        const fileContent = await this.readFileAsync(files[0]);
        if (files[0].type.includes("json") || files[0].type.includes("text"))
          this.setState({ fileType: "text" });
        else this.setState({ fileType: "binary" });
        this.setState({ fileContent });
        this.setState({
          updateFileContent: fileContent,
          showCompare: false,
          restoreValue: false,
        });
      }
    } catch (e) {
      console.log(e);
    }
  }

  readFileAsync(file) {
    return new Promise((resolve, reject) => {
      let reader = new FileReader();
      reader.onload = () => {
        resolve(reader.result);
      };

      reader.onerror = reject;
      reader.readAsText(file);
    });
  }

  private async handleSendCommand() {
    if (this.state.pending) {
      return;
    }

    this.setState({ commandStatus: "Executing" });
    const requireConfirmation = this.props.requireConfirmation;

    const message = `Confirm executing "${this.props.commandName}" on "${
      this.props.commandDevice
    }" with parameter as uploaded file ${
      this.state.fileContent !== this.state.updateFileContent ? `MODIFIED` : ``
    }`;
    if (!requireConfirmation || window.confirm(message)) {
      this.setState({ pending: true });
      if (this.props.command !== undefined)
        this.props.command.execute(this.state.updateFileContent.trim());
      else if (this.props.commandFromDevice !== undefined)
        this.props.commandFromDevice(
          this.props.commandName,
          this.state.updateFileContent.trim()
        );
      this.setState({ pending: false, commandStatus: "Executed" });
    }
  }

  /**
   * Supporting Functions
   */
  private truncate(str, len) {
    return str.length > len ? str.substr(0, len) + ".." : str;
  }

  onFilesError(error, file) {
    console.log(error, file);
  }
}

export default CommandArgsFile;
