import React, { Component } from "react";
import PropTypes from "prop-types";
import { Dropdown } from "react-bootstrap";
import { command } from "../../jive/propTypes";

export default class InputField extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleKeyDown = this.handleKeyDown.bind(this);
    this.handleExecute = this.handleExecute.bind(this);
    this.state = {
      value: "",
      valid:
        this.props.intype === "DevString" || this.props.intype === "DevVoid",
      defaultValue: "",
      defaultValidity: false,
    };
  }

  /**
   * This adds quotes to each element of input array
   *
   * @param {input is an array argument} input
   */
  addQuotes(input) {
    let returnVal = [];
    input.forEach((element) => {
      returnVal.push(element.trim());
    });
    return returnVal;
  }

  componentDidMount() {
    if ("custom" === this.props.renderType) this.getDefaultValue();
  }

  getDefaultValue() {
    if (this.props.commandArgs && this.props.commandArgs.length > 0) {
      this.props.commandArgs.forEach((arg) => {
        if (arg.isDefault && arg.value) {
          const response = this.validateData(arg.value, true);

          if (response.valid) {
            this.setState({
              value: response.data,
              valid: true,
              defaultValue: response.data,
              defaultValidity: true,
            });
          }
        }
      });
    }
  }

  /**
   * This is used to format input data as per the accepted type argument
   *
   * @param {value to be validated} input
   * @param {Parse as per accepted data type} acceptedType
   */
  validateInputArray(input, acceptedType) {
    let list = [];
    input = input.replace(/['"]*/g, "");

    if (input.indexOf(",") !== -1) {
      if ("[" === input[0]) input = input.substring(1, input.length - 1);

      let data = input.split(",");

      if (acceptedType.includes("String")) {
        if ("DevVarLongStringArray" === acceptedType) {
          const longArr = this.validateInputArray(
            input.substring(0, input.indexOf("]")),
            "Long"
          );
          const stringArr = this.validateInputArray(
            input.substring(input.indexOf("["), input.length) + "]",
            "String"
          );

          list = `[${longArr}],[${this.addQuotes(stringArr)}]`;
        } else if ("DevVarDoubleStringArray" === acceptedType) {
          const doubleArr = this.validateInputArray(
            input.substring(0, input.indexOf("]")),
            "Double"
          );
          const stringArr = this.validateInputArray(
            input.substring(input.indexOf("["), input.length) + "]",
            "String"
          );

          list = `[${doubleArr}],[${this.addQuotes(stringArr)}]`;
        } else if ("DevVarStringArray" === acceptedType) {
          const stringArr = this.validateInputArray(
            input.substring(input.indexOf("["), input.length),
            "String"
          );
          list = this.addQuotes(stringArr);
        } else {
          data.forEach((element) => {
            if (acceptedType.includes("Long") || acceptedType.includes("Short"))
              element = parseInt(element);
            else if (
              acceptedType.includes("Double") ||
              acceptedType.includes("Float")
            )
              element = parseFloat(element);

            list.push(element.toString());
          });
        }
      } else if (
        acceptedType.includes("Char") ||
        acceptedType.includes("Long") ||
        acceptedType.includes("Short")
      ) {
        data.forEach((element) => {
          list.push(Number.parseInt(element));
        });
      } else {
        data.forEach((element) => {
          list.push(Number.parseFloat(element));
        });
      }
    } else if (input === "[]") {
      list = [];
    } else {
      if (acceptedType.includes("String")) {
        list.push(input);
      } else {
        list.push(Number.parseFloat(input));
      }
    }

    return list;
  }

  validateData(data, strictCheck = false) {
    let response = {};

    if (this.props.intype === "DevBoolean" && data.length > 0) {
      response = { data: data, valid: true };
    } else if (data.length > 0 && this.props.intype !== "DevString") {
      if (this.props.intype.includes("Array")) {
        response = { data: data, valid: true };
      } else if (this.props.intype.includes("U") && data >= 0) {
        response = {
          data: parseInt(data, 10),
          valid: strictCheck ? !isNaN(parseInt(data, 10)) : true,
        };
      } else if (
        (this.props.intype.includes("Long") ||
          this.props.intype.includes("Short")) &&
        !this.props.intype.includes("U")
      ) {
        response = {
          data: parseInt(data, 10),
          valid: strictCheck ? !isNaN(parseInt(data, 10)) : true,
        };
      } else if (!this.props.intype.includes("U")) {
        response = {
          data: parseFloat(data, 10),
          valid: strictCheck ? !isNaN(parseFloat(data, 10)) : true,
        };
      }
    } else if (this.props.intype === "DevString") {
      response = { data: data, valid: true };
    } else {
      response = { data: "", valid: false };
    }

    return response;
  }

  handleChange(event) {
    const response = this.validateData(event.target.value, true);
    this.setState({ value: response.data, valid: response.valid });
  }

  handleKeyDown(e) {
    if (e.key === "Enter") {
      this.handleExecute(e);
    }
  }

  handleExecute(event) {
    event.preventDefault();
    let param = this.state.value;

    if (this.props.intype === "DevString") {
      param = String(this.state.value);
    } else if (this.props.intype === "DevBoolean") {
      param = this.state.value === "true" ? 1 : 0;
    } else if (this.props.intype.includes("Array")) {
      param = this.validateInputArray(this.state.value, this.props.intype);
    }

    this.props.onExecute(this.props.name, param);
    this.setState({ value: "" });
  }

  render() {
    const disabled = !(this.state.valid && this.props.isEnabled);
    const { intypedesc, intype, placeholder } = this.props;
    const displayPlaceHolder = placeholder==='intypedesc' ? intypedesc : intype;
    let inner = null;
    const buttonLabel = this.props.buttonLabel
      ? this.props.buttonLabel
      : "Execute";

    if (intype === "DevVoid") {
      return (
        <button
          style={{...this.props.btnCss}}
          className="btn btn-outline-secondary btn-sm"
          type="button"
          disabled={disabled}
          onClick={this.handleExecute}
        >
          {buttonLabel}
        </button>
      );
    }

    if (intype === "DevBoolean") {
      inner = (
        <select
          className="custom-select"
          id="inputGroupSelect04"
          value={this.state.value}
          onChange={this.handleChange}
        >
          <option value="" defaultValue disabled hidden>
            Choose...
          </option>
          <option value="true">True</option>
          <option value="false">False</option>
        </select>
      );
    } else if (intype.toLowerCase().includes("array")) {
      inner = (
        <input
          className="form-control"
          value={this.state.value}
          onKeyDown={this.handleKeyDown}
          onChange={this.handleChange}
          placeholder={displayPlaceHolder}
        />
      );
    } else if (intype.includes("U")) {
      inner = (
        <input
          type="number"
          min="0"
          className="form-control"
          value={this.state.value}
          onKeyDown={this.handleKeyDown}
          onChange={this.handleChange}
          placeholder={displayPlaceHolder}
        />
      );
    } else if (intype === "DevString") {
      inner = (
        <input
          type="text"
          className="form-control"
          value={this.state.value}
          onKeyDown={this.handleKeyDown}
          onChange={this.handleChange}
          placeholder={displayPlaceHolder}
        />
      );
    } else if (
      this.props.renderType &&
      "custom" === this.props.renderType.toLowerCase()
    ) {
      inner = (
        <Dropdown className="drp-wrapper" style={{ width: "160px" }}>
          <Dropdown.Toggle
            className="drp-bttn"
            variant="outline-secondary"
            style={{ width: "160px" }}
          >
            {this.state.value
              ? this.getNameFromValue(this.state.value)
              : "Select Argument"}
          </Dropdown.Toggle>

          <Dropdown.Menu>
            <Dropdown.Item
              key="resetSelection"
              selected={true}
              as="button"
              value="reset_selection"
              onClick={() =>
                this.setState({
                  value: this.state.defaultValue,
                  valid: this.state.defaultValidity,
                })
              }
            >
              Reset to default
            </Dropdown.Item>

            {this.props.commandArgs.length > 0 &&
              this.props.commandArgs
                .filter((arg) => undefined !== arg.value && arg.value !== "")
                .map((arg, idx) => {
                  const response = this.validateData(arg.value, true);

                  const label = arg.name || arg.value;
                  const title = response.valid
                    ? `Write value "${arg.value}"`
                    : `Invalid value "${arg.value}"`;

                  return (
                    <span key={idx} title={title}>
                      <Dropdown.Item
                        disabled={!response.valid}
                        as="button"
                        value={arg.value}
                        onClick={this.handleChange}
                      >
                        {label}
                      </Dropdown.Item>
                    </span>
                  );
                })}
          </Dropdown.Menu>
        </Dropdown>
      );
    } else {
      inner = (
        <input
          type="number"
          className="form-control"
          value={this.state.value}
          onKeyDown={this.handleKeyDown}
          onChange={this.handleChange}
          placeholder={displayPlaceHolder}
        />
      );
    }

    return (
      <div className="input-group">
        {inner}
        <div className="input-group-append">
          <button
            style={{...this.props.btnCss}}
            className="btn btn-outline-secondary btn-sm"
            type="button"
            disabled={disabled}
            onClick={this.handleExecute}
          >
            {buttonLabel}
          </button>
        </div>
      </div>
    );
  }

  getNameFromValue(value) {
    const argObj = this.props.commandArgs.filter(
      (arg) => String(arg.value) === String(value)
    );
    return argObj.length > 0 && argObj[0].name !== "" ? argObj[0].name : value;
  }
}

InputField.propTypes = {
  onExecute: PropTypes.func,
  commands: PropTypes.oneOfType([PropTypes.arrayOf(command), command]),
  name: PropTypes.string,
  intype: PropTypes.string,
  renderType: PropTypes.string,
  commandArgs: PropTypes.array,
};
