import tangoAPI from "../api/tangoAPI";
import config from "../../config.json";
import { WEBSOCKET } from "../../jive/state/actions/actionTypes";

export async function fetchInitialValues(dispatch, element) {
    const url = window.location.href.replace(config.basename,"").split('/');
    const attributes = await tangoAPI.fetchAttributesValues(
        url[3],
        element
    );

    if (attributes[0]) {
        const attribute = {
            device: attributes[0].device,
            attribute: attributes[0].name,
            value: attributes[0].value,
            timestamp: attributes[0].timestamp,
            quality: attributes[0].quality,
            writevalue: attributes[0].writevalue
        }
        const data = JSON.stringify({ payload: { data: { attributes: attribute } } });
        dispatch({
            type: WEBSOCKET.WS_MESSAGE,
            value: [{ data }]
        });
    }
}