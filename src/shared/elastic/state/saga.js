import { takeEvery, fork, call, put } from "redux-saga/effects";

import {
  fetchElasticLogsSucces,
  fetchElasticLogsError
} from "./actionCreators";

import {
  FETCH_ELASTIC,
} from "./actionTypes";

import ElasticAPI from "../api";

function* fetchElasticLog(action) {
  const result = yield call(ElasticAPI.fetchElasticLogs, action.filters);
  const resultAction = result ? fetchElasticLogsSucces(result) : fetchElasticLogsError();
  yield put(resultAction);
}

export function* fetchElasticLogsSaga() {
  yield takeEvery(FETCH_ELASTIC, fetchElasticLog);
}

export default function elasticLogsSaga() {
  return function* elasticLogs() {
    yield fork(fetchElasticLogsSaga);
  }
}
