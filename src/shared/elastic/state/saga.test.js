import {
    fetchElasticLogsSaga,
} from './saga';
// import {
//     fetchElasticLogsSucces,
//     fetchElasticLogsError
// } from "./actionCreators";
// import {
//     FETCH_ELASTIC,
//     FETCH_ELASTIC_SUCCESS,
//     FETCH_ELASTIC_ERROR,
// } from "./actionTypes";

const stepper = (fn) => (mock) => fn.next(mock).value

test('the fetchElasticLogs saga happy path', async () => {

    const mockFilters = { devices: "sys/test1", from: 0, size: 10 }
    const step = stepper(fetchElasticLogsSaga());

    const SageStep1take = step();
    expect(SageStep1take.type).toEqual('FORK');
    expect(SageStep1take.payload.args[0]).toEqual('FETCH_ELASTIC')

    // const SageStep2call = step();
    // const SageStep3call = step(mockFilters);
    // expect(SageStep2call.type).toEqual('CALL');

    // const reponse = [
    //     {
    //         "_id": "1234",
    //         "_source": {
    //             "stream": "stderr",
    //             "message": "{}",
    //         },
    //         "sort": [123456]
    //     }
    // ]

    // const SageStep3put = step(reponse);
    // expect(SageStep3put.type).toEqual('PUT');
    // expect(SageStep3put.payload.action).toEqual({ type: FETCH_ELASTIC_SUCCESS, results: reponse });
    // expect(SageStep3put.payload.action).toEqual(fetchElasticLogsSucces(reponse));

});

test('the fetchElasticLogs saga sad path', async () => {

    const mockFilters = { devices: "sys/test1", from: 0, size: 10 }
    const step = stepper(fetchElasticLogsSaga());

    const SageStep1take = step();
    expect(SageStep1take.type).toEqual('FORK');
    // expect(SageStep1take.payload).toEqual({ pattern: FETCH_ELASTIC });

    // const SageStep2call = step(mockFilters);
    // expect(SageStep2call.type).toEqual('CALL');

    // const SageStep3put = step();
    // expect(SageStep3put.type).toEqual('PUT');
    // expect(SageStep3put.payload.action).toEqual({ type: FETCH_ELASTIC_ERROR });
    // expect(SageStep3put.payload.action).toEqual(fetchElasticLogsError());

});
