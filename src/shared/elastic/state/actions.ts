import {
  FETCH_ELASTIC,
  FETCH_ELASTIC_ERROR,
  FETCH_ELASTIC_SUCCESS
} from "./actionTypes";

import { Action } from "redux";

export interface IFetchElasticLogsAction extends Action {
  type: typeof FETCH_ELASTIC;
  filters: object;
}

export interface IFetchElasticLogsSuccessAction extends Action {
  type: typeof FETCH_ELASTIC_SUCCESS;
  results: object;
}

export interface IFetchElasticLogsErrordAction extends Action {
  type: typeof FETCH_ELASTIC_ERROR;
}
